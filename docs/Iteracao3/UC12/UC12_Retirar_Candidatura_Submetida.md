# UC 12 - Retirar Candidatura Submetida

## 1. Engenharia de Requisitos

### Formato Breve

O *freelancer* inicia o processo de eliminação de candidatura submetida. O sistema apresenta as candidaturas submetidas pelo *freelancer*. O *freelancer* seleciona a candidatura que pretende eliminar. O sistema pede ao *freelancer* para confirmar. O *freelancer* confirma a eliminação e o sistema elimina e informa do sucesso da operação.

### SSD

![UC12_SSD](UC12_SSD.svg)

### Formato Completo

O *freelancer* inicia o processo de eliminação de candidatura submetida.

#### Ator principal

*Freelancer*.

#### Partes interessadas e seus interesses

* ***Freelancer*:** pretende conhecer a classificação das suas candidaturas à realização de determinados anúncios publicados na plataforma.
* **T4J:** pretende conhecer as candidaturas submetidas por *freelancers*.
* **Organização:** pretende que a lista de candidaturas de *freelancers* esteja sempre atualizada.
* **Colaborador de Organização:** pretende ter a lista de candidaturas aos anúncios publicados, atualizada.

#### Pré-condições

O *freelancer* ter candidaturas registadas para pelo menos um anúncio.

#### Pós-condições

Uma candidatura é eliminada da lista de candidaturas de *freelancer* a um anúncio publicado.

#### Cenário de sucesso principal (ou fluxo básico)

1. O *freelancer* inicia o processo de eliminação de candidatura submetida.
2. O sistema apresenta todos os anúncios a que o *freelancer* se candidatou e está associado através do email.
3. O *freelancer* seleciona o anúncio a que se candidatou e pretende eliminar.
4. O sistema apresenta a candidatura que o *freelancer* fez para o dado anúncio.
5. O *freelancer* seleciona a candidatura que pretende eliminar.
6. O sistema apresenta a candidatura, pedindo ao *freelancer* para confirmar. 
7. O *freelancer* confirma.
8. O sistema elimina a candidatura e informa o *freelancer* do sucesso da operação. 

#### Extensões (ou fluxos alternativos)

*a. O *freelancer* solicita o cancelamento do processo de eliminação de candidatura.
> O caso de uso termina.

#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência

\-

#### Questões em aberto

* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC12_MD](UC12_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal                                                                                                | Questão: Que Classe...                                 | Resposta                     | Justificação                                                   |
|:---------------------------------------------------------------------------------------------------------------|:-------------------------------------------------------|:-----------------------------|:---------------------------------------------------------------|
| 1. O *freelancer* inicia o processo de eliminação de candidatura submetida.                                    | ...interage com o utilizador?                          | RetirarCandidaturaUI         | Pure Fabrication                                               |
|         	                                                                                                     | ...coordena o UC?                                      | RetirarCandidaturaController | Controller                                                     |            
|        		                                                                                                 | ...conhece o utilizador/*freelancer* a usar o sistema? | SessaoUtilizador             | IE: cf. documentação do componente de gestão de utilizadores.  | 
|                                                                                                                | ...conhece o *freelancer*?                             | Plataforma                   | IE: no MD, a Plataforma tem todos os *Freelancers* registados. |
|  		                                                                                                         |	                                                      | RegistoFreelancer            | Por aplicação de HC+LC delega a *Freelancers*.                 | 
| 2. O sistema apresenta todos os anúncios a que o *freelancer* se candidatou e está associado através do email. | ...conhece os anúncios?				                  | Plataforma                   | IE: no MD, a Plataforma mostra todos os anúncios.              |
|        		                                                                                                 |					                                      | RegistoAnuncio               | Por aplicação de HC+LC delega a Anuncios.                      | 
| 3. O *freelancer* seleciona o anúncio a que se candidatou e pretende eliminar.                                 | N/A					                                  |                              |                                                                |
| 4. O sistema apresenta a candidatura que o *freelancer* fez para o dado anúncio.                               | ...conhece as candidaturas?                            | Anuncio                      | IE: no MD, o Anuncio tem as Candidaturas.                      |
|        		                                                                                                 |					                                	  | ListaCandidaturas            | Por aplicação de HC+LC delega a ListaCandidaturas.             | 
| 5. O *freelancer* seleciona a candidatura que pretende eliminar.                                               | N/A							                          |                              |                                                                |
| 6. O sistema apresenta a candidatura, pedindo ao *freelancer* para confirmar.                                  | N/A                                                    |                              |                                                                |  
| 7. O *freelancer* confirma.                                                                                    | N/A					                                  |                              |                                                                |
| 8. O sistema elimina a candidatura e informa o *freelancer* do sucesso da operação.                            | ...informa o *Freelancer*?	                          | RetirarCandidaturaUI         |                                                                |

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anuncio


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * RetirarCandidaturaUI  
 * RetirarCandidaturaController
 * RegistoFreelancer
 * RegistoAnuncio
 * ListaCandidaturas

###	Diagrama de Sequência

![UC12_SD](UC12_SD.svg)

###	Diagrama de Classes

![UC12_CD](UC12_CD.svg)
