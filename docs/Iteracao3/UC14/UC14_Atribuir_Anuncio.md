# UC14 - Adjudicar/Atribuir (manualmente) Anúncio 

## 1. Engenharia de Requisitos

### Formato Breve

O Gestor da organização inicia o processo de Adjudicar/Atribuir Anuncio. O Sitema mostra todos os anúncios seriados. O Gestor escolhe 1. O sistema preenche os dados
necessários para a criação da adjudicação (i.e Referência à Organização,o Freelancer, descrição da Tarefa, período de afeto, valor remonerário e uma
referência ao anúncio). O Gestor insere todos os dados. O Sistema gera um id único e sequêncial, regista a data se adjudicação, verifica os dados e pede confirmação.
O Gestor confirma. O sistema retorna o sucesso do caso de uso.


### SSD
![UC14-SSD](UC14_SSD.svg)


### Formato Completo

#### Ator principal

O Gestor da Organização

#### Partes interessadas e seus interesses

* **Gestor da Organização:** pretende que a seriação que realizou seja registada na plataforma.
* **Organização:** pretende que uma tarefa que necessite ser feita seja correspondida por um freelancer.
* ***Freelancer*:** pretende que seja contratado, cujo resultado será visualizado neste UC.
* **T4J:** pretende que a atribuição de tarefas das organizações aos *freelancers* seja facilitada.

#### Pré-condições

Uma seriação feita de um anúncio **[(Ver UC10 e UC13)](../DUC.md)**

#### Pós-condições
A adjudicação é guardada na plataforma com sucesso.

#### Cenário de sucesso principal (ou fluxo básico)

1. O Gestor Inicia o processo de adjudicação do anúncio. 
2. O Sistema Mostra todos os anúncios seriados.
3. O Gestor Escolhe um anúncio
4. O Sitema Pede os dados necessários (referência à Organização, referência ao freelancer, descrição da tarefa,data de afeto inicial, data de afeto final, valor a pagar, referência ao anúncio) . 
5. O Gestor Introduz os dados fornecidos. 
6. O Sistema Gera um id sequencial e regista a data de adjudicação.
7. O Sistema Cria a adjudicação, verifica, mostra ao Gestor e pede confirmação. 
8. O Gestor Confirma os dados. 
9. O Sistema Faz a reverificação e adição da adjudicação ao sistema mostrando o sucesso da operação. 


#### Extensões (ou fluxos alternativos)

* *a) O Gestor cancela o caso de uso:<br>

        O caso de uso termina
* 2a) Não existem anúncios seriados
        
        O Sistema redireciona o utilizador para UC10.
        O caso de uso termina.

*  5a) Os dados introduzidos estão incompletos ou inválidos:
>1. O sistema alerta o Gestor para o sucedido.
>2. O sistema permite o Gestor a introduzir os dados (Paço 2).

>
>          O Gestor não coloca os dados certos. Repete-se o paço 2. 
* 7a) A verificação é mal sucedida:       
>       O caso de uso termina.
* 8a) O Gestor não confirma os dados:
>      O caso de uso termina.
* 9a) A verificação não é bem sucedida:
>       O caso de uso termina.
#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência

\-

#### Questões em aberto

* Este caso de uso remove o Anúncio adjudicado do sistema?
* Qual é a frequência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC
![](UC14_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O Gestor Inicia o processo de adjudicação do anúncio. | ...interage com o utilizador?| AdjudicarAnuncioUI| Pure Fabrication pois não se justifica a atribuição dessa responsabilidade a outra classe presente no domínio
| |...cordena o caso de uso? | AdjudicarAnuncioController| Controller 
| |...cria a adjudicação?| RegistoAdjudicacao|  Creator (Regra 1); HC+LC; Pure Fabrication pois não se justifica a atribuição dessa responsabilidade a outra classe presente no domínio.|
| 2. O Sistema Mostra todos os anúncios seriados.|	...tem todos os anúncios seriados?| RegistarAnuncio| Creator(Regra 1); HC+LC                              |
| 3. O Gestor Escolhe um anúncio
| 4. O Sitema preenche os dados necessários.  		 |	...guarda os dados introduzidos? | Adjudicacao| Information Expert              |
| |...tem referencia à organização? | Plataforma | Information Expert
| |...tem a referência ao Freelancer? | Candidatura
| |...tem a descrição da tarefa? | Tarefa| IE
| |...tem o valor a pagar?| Candidatura|IE
| |...tem a referência ao anúncio?|Anuncio|IE
| 6. O Sistema Gera um id sequencial e regista a data de adjudicação.		 |	...gera o id e a data? |  Adjudicacao            | Information Expert                              |
| 7. O Sistema Cria a adjudicação, verifica, mostra ao Gestor e pede confirmação. |	...verifica a adjudicação?	| RegistoAdjudicacao | Creator(Regra 2)					 |             |                              |
| 8. O Gestor Confirma os dados introduzidos. |							 |             |                              |              
| 9. O Sistema Faz a reverificação e adição da adjudicação ao sistema mostrando o sucesso da operação. | ...verifica a adjudicação?|RegistoAdjudicacao| Creator
| | ...guarda as adjudicações? |RegistoAdjudicacao|Creator(Regra 1) 
### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Adjudicacao
 * Anuncio
 * Candidatura
 * Tarefa
 * Organizacao
 * Plataforma

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * AdjudicarAnuncioUI  
 * AdjudicarAnuncioController
 * RegistoAdjudicacao
 * RegistoAnuncio


###	Diagrama de Sequência

![SD_UCX.png](UC14_SD.svg)

* ref getDadosNecessarios

![](getDadosNecessários_SD.svg)


###	Diagrama de Classes

![CD_UCX.png](UC14_CD.svg)
