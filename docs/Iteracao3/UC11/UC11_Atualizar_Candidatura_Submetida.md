# UC 11 - Atualizar Candidatura Submetida

## 1. Engenharia de Requisitos

### Formato Breve
O Freelancer inicia a atualização de uma candidatura.
O sistema mostra a lista de candidaturas do freelancer.
O Freelancer seleciona uma das candidatura da lista.
O sistema mostra os dados introduzidos anteriormente e dá opção ao Freelancer de os atualizar.
O Freelancer atualiza os dados que pretende. O sistema valida e pede para confirmar. O Freelancer confirma
e o sistema informa do sucesso da operação e guarda os dados.


### SSD
![UC11_SSD.svg](UC11_SSD.svg)


### Formato Completo

#### Ator principal

Freelancer

#### Partes interessadas e seus interesses


* **Freelancer:** pretende atualizar candidatura .
* **Organização:** pretende ver as candidaturas atualizadas .

#### Pré-condições

Ter o Freelancer registado e haverem candidaturas já registadas pelo mesmo Freelancer que as quer atualizar.

#### Pós-condições
As candidaturas selecionadas são atualizadas e alteradas/guardadas no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O Freelancer inicia a atualização de uma candidatura.
2. O sistema mostra a lista de candidaturas do freelancer.
3. O Freelancer seleciona uma das candidatura da lista.
4. O sistema mostra os dados introduzidos anteriormente e dá opção ao Freelancer de os atualizar.
5. O Freelancer atualiza os dados que pretende. 
6. O sistema valida e apresenta os dados ao Freelancer e pede a sua confirmação. 
7. O Freelancer confirma.
8. O sistema regista os dados, atualiza a Candidatura e informa o Freelancer do sucesso da operação.


#### Extensões (ou fluxos alternativos)

* a) O Freelancer cancela o caso de uso:<br>

        O caso de uso termina
* 2a) Não existem candidaturas registadas.
        
        O Sistema redireciona o utilizador para UC9.
        O caso de uso termina.

*  5a) Os dados introduzidos são inválidos:
>1. O sistema alerta o freelancer para o sucedido.
>2. O sistema permite o freelancer a introduzir os dados (Paço 2).
>
>          O freelancer não coloca os dados certos. Repete-se o paço 2. 
* 6a) A verificação é mal sucedida:       
>       O caso de uso termina.
* 7a) O Freelancer Colaborador não confirma os dados:
>      O caso de uso termina.
#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência

\-

#### Questões em aberto

* Qual é a informação da candidatura que o Freelancer pode alterar? Toda?
* A candidatura pode ser atualizada fora do prazo (i.e. período de candidatura)?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC11_MD.svg](UC11_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| O Freelancer inicia a atualização de uma candidatura. |... interage com o utilizador?|AtualizarCandidaturaUI|Pure Fabrication|
| |...coordena o UC?	 				| AtualizarCandidaturaController	| Controller    |
| |...conhece o utilizador/Freelancer a usar o sistema?	|SessaoUtilizador|IE: cf. documentação do componente de gestão de utilizadores.|
| |...conhece o Freelancer ?				|Plataforma|conhece todos os Freelancers|
| | 						     | Anúncio		| O Anúncio é relativo a uma Tarefa | IE: no MD o Anúncio é relativo a uma Tarefa |
| | 						     | Tarefa		| A Tarefa tem uma Categoria de Tarefa associada | IE: no MD a Tarefa é relativa a uma Categoria de Tarefa |
| O sistema mostra a lista de candidaturas do freelancer.|... conhece as Candidaturas do Freelancer? |ListaCandidaturas | Por aplicação de HC+LC delega a ListaCandidaturas|
| O Freelancer seleciona uma das candidatura da lista. |							 |             |                              |
| O sistema mostra os dados introduzidos anteriormente e dá opção ao Freelancer de os atualizar. |...conhece os dados da Candidatura?|Candidatura|IE: Candidatura conhece os seus dados  |
| O Freelancer atualiza os dados que pretende.|	...guarda os dados introduzidos? |Anuncio| No MD Anúncio recebe Candidaturas|
| |							| ListaCandidaturas | Por aplicação de HC+LC delega a ListaCandidaturas|
| |							| Candidatura | IE: Candidatura conhece os seus dados|
| O sistema valida e apresenta os dados ao Freelancer e pede a sua confirmação.|... valida os dados da Candidatura (validação global)?| ListaCandidaturas| IE: no MD o Anúncio recebe Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas|
| O Freelancer confirma.		 |							 |             |                              |              
| O sistema regista os dados e cria a Candidatura e informa o Freelancer do sucesso da operação. |...guarda a Candidatura?| Anúncio|IE: no MD o Anúncio recebe Candidaturas.|
| |							| ListaCandidaturas | IE: no MD o Anúncio recebe Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas|
| |...informa o colaborador?|AtualizarCandidaturaUI|
### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Candidatura
 * Freelancer
 * Anúncio
 * Tarefa

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * AtualizarCandidaturaUI  
 * AtualizarCandidaturaController
 * ListaCandidaturas
 * RegistoFreelancer
 * RegistoAnuncios


###	Diagrama de Sequência

![UC11_SD.svg](UC11_SD.svg)


###	Diagrama de Classes

![UC11_CD.svg](UC11_CD.svg)
