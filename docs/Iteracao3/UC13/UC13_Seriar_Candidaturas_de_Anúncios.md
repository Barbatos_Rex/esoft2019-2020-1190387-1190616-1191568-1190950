# UC 13 - Seriar Candidatura de Anuncios

## 1. Engenharia de Requisitos

### Formato Breve

Quando é alcançado o horario de proceso de seriação automaticca diaria e concluido o proceso deseriação nao automatico realiazaado pelo colaborador, o sistema identifica os anuncios cujo o tipo de regimento estipula que os criterio de seriação sejam objetivos. Assim os sistema irá seriar as candidaturas que estejam dentro do periodo de seriação e que ainda nao estejam seriadas eram ser avaliadas de forma automática.
No final o sistema irá guardar as candidaturas seriadas.

### SSD
![UC13_SSD](UC13_SSD.svg)

### Formato Completo

#### Ator principal

* **Timer**

#### Partes interessadas e seus interesses

* **Freelancer:** pretende conhecer a classificação das suas candidaturas à realização de determinados anúncios publicados na plataforma.
* **Organização:** pretende contratar pessoas externas (outsourcing) para a realização de determinadas tarefas e com competências técnicas apropriadas.
* **T4J:** pretende satisfazer as organizações e os freelancer facilitando a contratação de freelancers pelas organizações e vice-versa.

#### Pré-condições
Existir menos uma candidatura feita pelo freelancer **[(Ver UC9)](../UC9/UC9_EfetuarCandidatura.md)**

#### Pós-condições
As candidaturas submetidas à seriação automatica são registas de acordo com a classificação atribuida.
#### Cenário de sucesso principal (ou fluxo básico)
1. O horário de seriação automática é atingido.
2.O sistema inicia seriação aos anuncios que qumpram os requesitose que estejam no periodo de seriação e regista os dados.

#### Extensões (ou fluxos alternativos)

* 1a) Não existem candidaturas para serem seriadas.
 > O caso de uso termina.
* 2a) 0 Anuncio não cumpre os requesitos
  > O caso de uso termina.   

* 2b)nao existem anuncios que estejam no periodo de seriação
> O caso de uso termina.

* 2c)Nao exitem anuncios que ainda nao tenham sido seriados
> O caso de uso termina.
    
                                        
#### Requisitos especiais
(enumerar requisitos especiais aplicáveis apenas a este UC)

\-

#### Lista de Variações de Tecnologias e Dados
(enumerar variações de tecnologias e dados aplicáveis apenas a este UC)
\-

#### Frequência de Ocorrência
Esta UC é realizada todos os dias à hora programada

#### Questões em aberto

* Quando e como é definida a hora a que ocorre o processo automatico?
* O processo automatico posso ser desativado?
* Como é que sabemos que a seriação foi efetuada?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC
![UC13_MD](UC13_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
|**1. O horário de seriação automática é atingido** | ...coordena o UC?     |   **SeriarCandidaturaAnuncioTask**       |      Task  |
|| ... cria a instância da Task?  | **Plataforma**|   Creator (Regra 1): Plataforma contem a Task       |
|| ... cria a instância do Timer?  | **Plataforma**|   Creator (Regra 3): a Plataforma tem os dados necessários para inicializar o Timer        |
||   ... determina que o tempo foi atingido e despoleta o processo de seriação automática? |     **Timer**       |   Timer        |
|**2.O sistema inicia seriação aos anuncios que qumpram os requesitose que estejam no periodo de seriação e regista os dados.** | ...conhece RegistoAnuncio de tarefas publicadas?		| **Plataforma** | IE: Plataforma contém/agrega RegistoAnuncio (segundo Padrão HC+LC sobre Plataforma)  |       
|| ...possui a candidatura?   | **ListaCandidaturas**      | IE: Lista de Candidaturas contem candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas |
|| ...conhece a classe Anuncio? |  **RegistoAnuncio** | IE: RegistoAnuncio contém/agrega Anuncio (segundo Padrão HC + LC) |
|| ...conhece a classe ProcessoSeriacao? |**Anuncio**| IE:o Anuncio conhece os seus processos de seriação |
|| ...cria a instância do processo de seriação?  | **Anuncio**    |  Creator (Regra1): no MD o anuncio espoleta o processo de seriação |
|| ...cria, valida e adiciona a instancia da classificação? | **ProcessoSeriacao** | creator: no MD a classificacao é resultante do processo de seriação |
||       |**Anuncio**|IE: no MD o Anúncio recebe as seriações.   |
|| ...regista o processo de seriação? | **Anuncio** | IE: o Anuncio contém os processos de seriação. |
|| ...conhece o tipo de regimento|**Anuncio**|IE: no MD o Anúncio recebe TipoRegimento.  |
|| ... verifica a obrigatoriedade da atribuição?    | **TipoRegimento**       | IE: o Tipo de Regimento contém a obrigatoriedade. |   
|| ...cria a instância do Processo de Atribuição?    | **Anuncio** | creator: no MD o processo de adjudicacao está associado ao anúncio.|
|| ...possui o Freelancer? | **Candidatura** | IE: no MD a candidatura é efetuada pelo Freelancer |
|| ...possui o Registo de Organizacao? | **Plataforma** | IE:a plataforma possui organizacao. |
|| ...possui Organizacao? | **RegistoOrganizacoes** | IE: RegistoOrganizacao contém/agrega Organizacao (segundo Padrão HC + LC)
|| ...possui a tarefa? | **Anuncio** | IE: anuncio tem tarefa |
|| ...cria a adjudicação | **ProcessoAtribuicao** | creator: Anuncio tem atribuição |
|| ...valida a adjudicação? | **ProcessoAtribuicao** |  Creator (Regra1): no MD o anuncio espoleta o processo de seriação   | 
|| ...vai guardar a adjudicação?|**ProcessoAtribuicao**| IE: o ProcessoAtribuicao contém as adjudicações. |
### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Classe1
 * Classe2
 * Classe3

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * xxxxUI  
 * xxxxController


###	Diagrama de Sequência

![UC13_SD.svg](UC13_SD.svg)
![UC13_SD_seriarCandidaturasSubmetidas.svg](UC13_SD_seriarCandidaturasSubmetidas.svg)
![UC13_SD_seriarCandidaturasSubmetidas.svg](UC13_SD_seriarCandidaturasSubmetidas.svg)

###	Diagrama de Classes

![UC13_CD.svg](UC13_CD.svg)

