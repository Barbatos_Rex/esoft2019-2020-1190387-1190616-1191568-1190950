 /*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
  */
 package pt.ipp.isep.dei.esoft.pot.model;

 import java.util.HashSet;
 import java.util.Objects;
 import java.util.Set;

 /**
  * @author paulomaio
  */
 public class Organizacao {
     private final String m_strNome;
     private final String m_strNIF;
     private final EnderecoPostal m_oEnderecoPostal;
     private final String m_strWebsite;
     private final String m_strTelefone;
     private final String m_strEmail;
     private final String m_strPassword;
     private final Colaborador m_oGestor;
     private final Set<Colaborador> m_lstColaboradores = new HashSet<Colaborador>();
     private final RegistoAnuncio anuncios;
     private RegistoTarefa tarefas;
     private RegistoColaborador colaboradores;

     public Organizacao(String strNome, String strNIF, String strWebsite, String strTelefone,
                        String strEmail, EnderecoPostal oMorada, Colaborador oColaborador, String strpassword) {
         if ((strNome == null) || (strNIF == null) || (strTelefone == null) ||
                 (strEmail == null) || (oMorada == null) || (oColaborador == null) || (strpassword == null) ||
                 (strNome.isEmpty()) || (strNIF.isEmpty()) || (strTelefone.isEmpty()) ||
                 (strEmail.isEmpty()) || (strpassword.isEmpty()))
             throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

         this.m_strNome = strNome;
        this.m_strNIF = strNIF;
        this.m_oEnderecoPostal = oMorada;
        this.m_strWebsite = strWebsite;
        this.m_strTelefone = strTelefone;
        this.m_strEmail = strEmail;
        this.m_oGestor = oColaborador;
        this.m_strPassword=strpassword;
         this.m_lstColaboradores.add(oColaborador);
         this.anuncios = new RegistoAnuncio();

    }
    
    public Colaborador getGestor()
    {
        return this.m_oGestor;
    }
   
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.m_strNIF);
        return hash;
    }
    
    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        Organizacao obj = (Organizacao) o;
        return (Objects.equals(m_strNIF, obj.m_strNIF));
    }
    
    @Override
    public String toString() {
        String str = String.format("%s - %s - %s - %s - %s - %s - %s", this.m_strNome, this.m_strNIF, this.m_strWebsite, this.m_strTelefone, this.m_strEmail, this.m_oEnderecoPostal.toString(), this.m_oGestor.toString());
        return str;
    }

     public static EnderecoPostal novoEnderecoPostal(String strLocal, String strCodPostal, String strLocalidade) {
         return new EnderecoPostal(strLocal, strCodPostal, strLocalidade);
     }

     public Colaborador novoColaborador(String strNome, String strFuncao, String strTelefone, String strEmail, String strPassword) {
         return new Colaborador(strNome, strFuncao, strTelefone, strEmail, strPassword);
     }


     public Tarefa novaTarefa(String ref, String dsInformal, String dsTecnica, double dur, double cust, CategoriaTarefa cat) {
         return new Tarefa(ref, dsInformal, dsTecnica, dur, cust, cat);
     }

     public boolean validaTarefa(Tarefa tarefa) {
         return tarefa != null && tarefa.getRef() != null;
     }

    public boolean validaColaborador(Colaborador colaborador){
        boolean validaColaborador=false;
                if(colaborador.getNome()!=null && colaborador.getEmail()!=null && colaborador.getFuncao()!=null &&
                        colaborador.getTelefone() != null && colaborador.getNome().isEmpty() && colaborador.getEmail().isEmpty()
                        && colaborador.getFuncao().isEmpty() && colaborador.getTelefone().isEmpty()) {
                    validaColaborador = true;

                }
        return validaColaborador;
    }

     public boolean RegistoColaborador(Colaborador colaborador) {
         return this.m_lstColaboradores.add(colaborador);
     }


     public RegistoTarefa getTarefas() {
         return tarefas;
     }

     public RegistoAnuncio getAnuncios() {
         return this.anuncios;
     }

     public RegistoColaborador getRegistoColaborador() {
         return colaboradores;
     }

     public String getEmailOrg() {
         return this.m_strEmail;
     }
 }
