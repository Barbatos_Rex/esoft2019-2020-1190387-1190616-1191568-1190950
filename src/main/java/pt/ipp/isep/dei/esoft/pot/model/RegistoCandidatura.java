package pt.ipp.isep.dei.esoft.pot.model;

public class RegistoCandidatura {
    private int valor;
    private int nrDias;
    private static String texto;
    Candidatura candidatura;
    public void novaCandidatura(){
        candidatura = new Candidatura(valor, nrDias, texto);
    }
    public void validaCandidatura(){
        if(valor>0||nrDias>0){
            System.out.println("Candidatura aceite");
        }else {
            System.out.println("Candidatura inválida");
        }
    }
}
