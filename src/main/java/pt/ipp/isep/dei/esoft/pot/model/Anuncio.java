package pt.ipp.isep.dei.esoft.pot.model;

import java.util.List;

public class Anuncio {
	private static int numeroDeAnuncios;
	private String emailOrg;
	private Tarefa tar;
	private static String identification;
	private Periodo pDePub;
	private Periodo pDeCand;
	private Seriacao ser;
	private Periodo pDeDec;
	private final String id;
	ListaCandidaturas listaCandidaturas;


	ProcessoSeriacao processoSeriacao;



	public Anuncio() {
		this.id = gerarId();
	}

	private static String gerarId() {
		return Integer.toString(numeroDeAnuncios++);
	}

	public static String getIdentification() {
		return identification;
	}


	public void setTarefa(Tarefa tar) {
		this.tar = tar;
	}

	public boolean verificarAnuncio() {
		return ser != null && tar != null && pDePub != null && pDeCand != null && pDeDec != null;
	}

	public boolean setPeriodoDePub(String periodoDePubIn, String periodoDePubFin) throws Exception {
		this.pDePub = new Periodo(periodoDePubIn, periodoDePubFin);
		return true;
	}

	public void setPeriodoDeCand(String periodoDeCandIn, String periodoDeCandFin) throws Exception {
		this.pDeCand = new Periodo(periodoDeCandIn, periodoDeCandFin);
	}

	public void setSeriacao(Seriacao ser) {
		this.ser = ser;
	}

	public void setEmailOrg(String emailOrg) {
		this.emailOrg = emailOrg;
	}

	public String getEmailOrg() {
		return emailOrg;
	}

	public Tarefa getTarefa() {
		return tar;
	}


	public Seriacao getSeriacao() {
		return ser;
	}

	public void setPeriodoDeDec(String periodoDeDecIn, String periodoDeDecFin) throws Exception {
		this.pDeDec = new Periodo(periodoDeDecIn, periodoDeDecFin);
	}

	public Periodo getpDePub() {
		return pDePub;
	}

	public Periodo getpDeCand() {
		return pDeCand;
	}

	public Periodo getpDeDec() {
		return pDeDec;
	}

	public String getId() {
		return id;
	}


	public void atualizarCandidatura(Candidatura candidaturaSelecionada,String anuncioId, double valorPrt, int nrDias, String txtApres, String txtMotiv) {
		listaCandidaturas.atualizarCandidatura(candidaturaSelecionada, valorPrt, nrDias, txtApres, txtMotiv);

	}

	public void setProcessoSeriacao(ProcessoSeriacao processoSeriacao) {
		this.processoSeriacao = processoSeriacao;
	}

	public Candidatura getCandidaturaNaPosicao(int posicao) {
		return listaCandidaturas.getCandidaturaNaPosicao(posicao);
	}

	public List<Candidatura> getCandidaturas() {
		return listaCandidaturas.getCandidaturasDoFreelancer();

	}
	public Candidatura getMelhorCandidatura(){
		Candidatura melhorCandidatura/**Será a primeira da Lista apos a seriacao*/ = null;
		return getCandidaturaNaPosicao(0);}
	public ProcessoSeriacao getNovoProcessoSeriacao() {
		return processoSeriacao;
	}
	public void registaProcessoSeriacao(ProcessoSeriacao ps){};
	public void validaProcessoSeriacao(ProcessoSeriacao ps){};

}
