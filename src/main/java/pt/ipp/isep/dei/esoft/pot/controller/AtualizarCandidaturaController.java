package pt.ipp.isep.dei.esoft.pot.controller;

import pt.ipp.isep.dei.esoft.pot.model.*;

import java.util.List;

public class AtualizarCandidaturaController {

    ListaCandidaturas listaCandidaturas;
    Plataforma plataforma;
    String emailDoUser;

    public AtualizarCandidaturaController() {

        if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_FREELANCER))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.plataforma = AplicacaoPOT.getInstance().getPlataforma();
         emailDoUser = AplicacaoPOT.getInstance().getSessaoAtual().getEmailUtilizador();

    }

    public List<Candidatura> getCandidaturasDoFreelancer() throws Exception {
        return listaCandidaturas.getCandidaturasDoFreelancer(RegistoFreelancer.getFreelancerByEmail(emailDoUser));

    }

    public Candidatura getCandidaturaById(String candId) {
        return listaCandidaturas.getCandidaturaById(candId);
    }

    public void atualizarCandidatura(Candidatura candidaturaSelecionada, String anuncioId, double valorPrt, int nrDias, String txtApres, String txtMotiv) {
        Anuncio anuncio =plataforma.getRegistoAnuncio().getAnuncioById(anuncioId);
        anuncio.atualizarCandidatura(candidaturaSelecionada, anuncioId, valorPrt, nrDias, txtApres, txtMotiv);
    }

}
