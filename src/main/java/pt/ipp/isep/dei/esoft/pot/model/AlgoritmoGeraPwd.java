package pt.ipp.isep.dei.esoft.pot.model;

public interface AlgoritmoGeraPwd {

    String geraPwd(String nome, String email);
}
