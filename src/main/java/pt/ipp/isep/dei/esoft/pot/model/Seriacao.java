package pt.ipp.isep.dei.esoft.pot.model;

public enum Seriacao {
	OPCIONAL {
		@Override
		public String toString() {
			return "Seriação Opcional";
		}
	},
	OBRIGATORIO {
		@Override
		public String toString() {
			return "Seriação Obrigatória";
		}
	},
	AUTOMATICO {
		@Override
		public String toString() {
			return "Seriação Automática";
		}
	};

	public static Seriacao getSerByRef(String seriacaoRef) {
		if (seriacaoRef.equals("opcional")) {
			return OPCIONAL;
		}
		if (seriacaoRef.equals("obrigatorio")) {
			return OBRIGATORIO;
		}
		if (seriacaoRef.equals("automatico")) {
			return AUTOMATICO;
		}
		return null;
	}

	public static Seriacao getSeriacao() {

	}
}
