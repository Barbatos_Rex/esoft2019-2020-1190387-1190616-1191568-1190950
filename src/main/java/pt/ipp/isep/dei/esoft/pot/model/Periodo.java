package pt.ipp.isep.dei.esoft.pot.model;

public class Periodo {
    private final int anoInicial;
    private final int mesInicial;
    private final int diaInicial;
    private final int horaInicial;
    private final int minutoInicial;
    private final int segundoInicial;
    private final int anoFinal;
    private final int mesFinal;
    private final int diaFinal;
    private final int horaFinal;
    private final int minutoFinal;
    private final int segundoFinal;

    public Periodo(String dataInicial, String dataFinal) throws Exception {
        //Supondo que ambas as datas serão datas no formato DD/MM/AAAA HH:MM:SS:

        //<editor-fold desc="Separação da data e do tempo">
        String[] dataCompletaI = dataInicial.split(" ");
        String[] dataCompletaF = dataFinal.split(" ");
        //</editor-fold>
        if (dataCompletaF.length != dataCompletaI.length && dataCompletaI.length != 2) {
            throw new Exception("Uma (ou mais) data[s] não obdece[m] ao formato DD/MM/AAAA HH:MM:SS!");
        }
        //<editor-fold desc="Arrays das datas">
        String dataI = dataCompletaI[0];
        String dataF = dataCompletaF[0];
        //</editor-fold>

        //<editor-fold desc="Separação da data inicial">
        String[] dataSeparadaI = dataI.split("/");

        this.diaInicial = Integer.parseInt(dataSeparadaI[0]);
        this.mesInicial = Integer.parseInt(dataSeparadaI[1]);
        this.anoInicial = Integer.parseInt(dataSeparadaI[2]);
        //</editor-fold>

        //<editor-fold desc="Separação da data final">
        String[] dataSeparadaF = dataF.split("/");

        this.diaFinal = Integer.parseInt(dataSeparadaF[0]);
        this.mesFinal = Integer.parseInt(dataSeparadaF[1]);
        this.anoFinal = Integer.parseInt(dataSeparadaF[2]);
        //</editor-fold>

        //<editor-fold desc="Arrays dos tempos">
        String tempoI = dataCompletaI[1];
        String tempoF = dataCompletaF[1];
        //</editor-fold>

        //<editor-fold desc="Separação do tempo inicial">
        String[] tempoSeparadoI = tempoI.split("/");

        this.horaInicial = Integer.parseInt(tempoSeparadoI[0]);
        this.minutoInicial = Integer.parseInt(tempoSeparadoI[1]);
        this.segundoInicial = Integer.parseInt(tempoSeparadoI[2]);
        //</editor-fold>

        //<editor-fold desc="Separação do tempo final">
        String[] tempoSeparadoF = tempoF.split("/");

        this.horaFinal = Integer.parseInt(tempoSeparadoF[0]);
        this.minutoFinal = Integer.parseInt(tempoSeparadoF[1]);
        this.segundoFinal = Integer.parseInt(tempoSeparadoF[2]);
        //</editor-fold>
    }

    //<editor-fold desc="Getters não necessários para a realização da UC8 e não previstos no CD">
    public int getAnoInicial() {
        return anoInicial;
    }

    public int getMesInicial() {
        return mesInicial;
    }

    public int getDiaInicial() {
        return diaInicial;
    }

    public int getHoraInicial() {
        return horaInicial;
    }

    public int getMinutoInicial() {
        return minutoInicial;
    }

    public int getSegundoInicial() {
        return segundoInicial;
    }

    public int getAnoFinal() {
        return anoFinal;
    }

    public int getMesFinal() {
        return mesFinal;
    }

    public int getDiaFinal() {
        return diaFinal;
    }

    public int getHoraFinal() {
        return horaFinal;
    }

    public int getMinutoFinal() {
        return minutoFinal;
    }

    public int getSegundoFinal() {
        return segundoFinal;
    }
    //</editor-fold>
}
