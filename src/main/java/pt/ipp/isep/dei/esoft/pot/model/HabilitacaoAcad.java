package pt.ipp.isep.dei.esoft.pot.model;

public class HabilitacaoAcad {
    private String grau;
    private String designacao;
    private String instituicao;
    private String media;

    public String getGrau() {
        return grau;
    }

    public void setGrau(String grau) {
        this.grau = grau;
    }

    public String getDesignacao() {
        return designacao;
    }

    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    public String getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(String instituicao) {
        this.instituicao = instituicao;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public HabilitacaoAcad(String grau, String designacao, String instituicao, String media){
        if ((grau == null) || (designacao == null) || (instituicao == null) ||(media == null) ||
                (grau.isEmpty()) || (designacao.isEmpty()) || (instituicao.isEmpty())|| (media.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

        this.grau = grau;
        this.designacao = designacao;
        this.instituicao = instituicao;
        this.media = media;
    }
}
