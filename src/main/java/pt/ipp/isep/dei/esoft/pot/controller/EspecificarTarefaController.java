package pt.ipp.isep.dei.esoft.pot.controller;

import pt.ipp.isep.dei.esoft.pot.model.*;

import java.util.ArrayList;

public class EspecificarTarefaController {

    private Plataforma plataforma;
    private Tarefa tarefa;
    private Organizacao org;

    public EspecificarTarefaController() {
        if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.plataforma = AplicacaoPOT.getInstance().getPlataforma();
        String emailDoUser = AplicacaoPOT.getInstance().getSessaoAtual().getEmailUtilizador();
        this.org = this.plataforma.getOrganizacaoByEmail(emailDoUser);
    }

    public ArrayList<CategoriaTarefa> getCategorias() {
        try {
            return (ArrayList<CategoriaTarefa>) plataforma.getCategorias();
        } catch (RuntimeException e) {
            return null;
        }
    }

    public boolean novaTarefa(String ref, String dsInformal, String dsTecnica, double dur, double cust, String catID) {
        try {
            tarefa = org.novaTarefa(ref, dsInformal, dsTecnica, dur, cust, this.plataforma.getCategoriasByRef(catID));
            return org.validaTarefa(tarefa);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean registaTarefa() {
        if (org.validaTarefa(tarefa)) {
            return org.add(tarefa);
        }
        return false;
    }





}
