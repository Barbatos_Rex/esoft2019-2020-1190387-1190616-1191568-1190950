package pt.ipp.isep.dei.esoft.pot.controller;

import pt.ipp.isep.dei.esoft.pot.model.*;

import java.util.ArrayList;
import java.util.List;

public class PublicarTarefaController {
	private RegistoAnuncio rAnun;
	private RegistoTarefa rTar;
	private final Organizacao org;
	private Tarefa tar;
	private Anuncio anun;
	private int periodoDePub;
	private int periodoDeCan;
	private int periodoDeDec;
	private Seriacao ser;

	public PublicarTarefaController() {
		if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO))
			throw new IllegalStateException("Utilizador não Autorizado");
		this.org = AplicacaoPOT.getInstance().getPlataforma().getOrganizacaoByEmail(AplicacaoPOT.getInstance().getSessaoAtual().getEmailUtilizador());
	}

	public boolean createAnuncio(String seriacaoRef) {
		try {
			this.rAnun = org.getAnuncios();
			this.anun = new Anuncio();
			this.anun.setEmailOrg(org.getGestor().getEmail());
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean getTarefas() {
		try {
			List<String> listaTarefasString = new ArrayList<>();
			this.rTar = org.getTarefas();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean getTarefaByRef(String ref) {
		try {
			this.tar = this.rTar.getTarefaById(ref);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean setDados(String pDePubIn, String pDePubFin, String pDeCandIn, String pDeCandFin, String pDeDecIn, String pDeDecFin) {
		try {
			this.anun.setPeriodoDePub(pDePubIn, pDePubFin);
			this.anun.setPeriodoDeCand(pDeCandIn, pDeCandFin);
			this.anun.setPeriodoDeDec(pDeDecIn, pDeDecFin);
			return true;
		} catch (Exception e) {
			return false;
		}
	}


	public boolean setSeriacao(String refReg) {
		try {
			this.ser = Seriacao.getSerByRef(refReg);
			this.anun.setSeriacao(ser);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean addAnuncio() {
		try {
			return rAnun.addAnuncio(this.anun);
		} catch (Exception e) {
			return false;
		}
	}
}
