package pt.ipp.isep.dei.esoft.pot.controller;

import pt.ipp.isep.dei.esoft.pot.model.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AdjudicarAnuncioController {

	private final Plataforma plat;
	private RegistoOrganizacao rOrg;
	private Organizacao org;
	private RegistoAnuncio rAn;
	private RegistoAdjudicacao rAdj;
	private Anuncio an;
	private Candidatura cand;
	private Freelancer free;
	private Tarefa tar;
	private Date dataAfetoInicial;
	private String descTarefa;
	private String emailOrg;
	private Date dataAfetoFinal;
	private double valorAPagar;
	private String refAnuncio;
	private Adjudicacao adj;

	public AdjudicarAnuncioController() {
		this.plat = AplicacaoPOT.getInstance().getPlataforma();
	}

	public List<String> lAnunciosString() {
		try {
			List<String> listaAnuncios = new ArrayList<>();
			this.rOrg = plat.getRegistoOrganizacao();
			this.org = rOrg.getOrganizacaoByEmail(AplicacaoPOT.getInstance().getSessaoAtual().getEmailUtilizador());
			this.rAn = plat.getRegistoAnuncio();
			this.rAdj = plat.getRegistoAdjudicacao();
			for (Anuncio a : RegistoAnuncio.getAnuncios()) {
				if (a.getEmailOrg().equals(org.getEmailOrg())) {
					listaAnuncios.add(a.toString());
				}
			}
			return listaAnuncios;
		} catch (Exception e) {
			return null;
		}
	}

	public String gerarAdjudicacao(String refAnuncio) {
		try {
			this.an = rAn.getAnuncioById(refAnuncio);
			this.cand = an.getCandidaturaNaPosicao(0);
			this.free = cand.getFreelancer();
			this.tar = an.getTarefa();
			getDadosNecessarios();
			this.adj = rAdj.novaAdjudicacao(this.emailOrg, free, descTarefa, dataAfetoInicial, dataAfetoFinal, valorAPagar, refAnuncio);
			rAdj.verificaAdjudicacao(adj);
			return adj.toString();
		} catch (Exception e) {
			return null;
		}
	}

	private void getDadosNecessarios() {
		this.emailOrg = org.getEmailOrg();
		this.descTarefa = tar.getDsInformal();
		this.dataAfetoInicial = new Date();
		dataAfetoInicial.setDate(dataAfetoInicial.getDate() + 1);
		this.dataAfetoFinal = new Date();
		dataAfetoFinal.setDate(dataAfetoInicial.getDate() + cand.getNrDias());
		this.valorAPagar = cand.getValor();
		this.refAnuncio = an.getId();
	}

	public boolean addAdjudicacao() {
		try {
			this.rAdj.verificaAdjudicacao(adj);
			return rAdj.addAdjudicacao(adj);
		} catch (Exception e) {
			return false;
		}
	}


}
