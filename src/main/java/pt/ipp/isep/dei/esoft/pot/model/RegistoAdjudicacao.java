package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RegistoAdjudicacao {
	private final List<Adjudicacao> adjudicacoes;

	public RegistoAdjudicacao() {
		this.adjudicacoes = new ArrayList<>();
	}

	public Adjudicacao novaAdjudicacao(String refOrg, Freelancer freelancer, String descTarefa, Date dataAfetoInicial, Date dataAfetoFinal, double valorAPagar, String refAnuncio) {
		return new Adjudicacao(refOrg, freelancer, descTarefa, dataAfetoInicial, dataAfetoFinal, valorAPagar, refAnuncio);
	}

	public boolean verificaAdjudicacao(Adjudicacao adj) {
		return adj.verificaAdjudicacao(adj);
	}

	public boolean addAdjudicacao(Adjudicacao adj) {
		return adjudicacoes.add(adj);
	}
}
