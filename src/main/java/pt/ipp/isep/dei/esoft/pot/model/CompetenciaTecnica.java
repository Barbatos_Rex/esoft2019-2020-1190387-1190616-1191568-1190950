/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Objects;

/**
 * @author paulomaio
 */
public class CompetenciaTecnica {
    private String descBreve;
    private String descDetalhada;
    private String id;
    private AreaAtividade area;

    public CompetenciaTecnica(String descBreve, String descDetalhada, String id, AreaAtividade area) {
        if ((descBreve == null) || (descDetalhada == null) || (id == null) ||
                (area == null) || (descBreve.isEmpty()) || (descDetalhada.isEmpty()) || (id.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

        this.descBreve = descBreve;
        this.descDetalhada = descDetalhada;
        this.id = id;
        this.area = area;
    }
    
    
    public boolean hasId(String strId)
    {
        return this.descBreve.equalsIgnoreCase(strId);
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.descBreve);
        return hash;
    }
    
    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        CompetenciaTecnica obj = (CompetenciaTecnica) o;
        return (Objects.equals(descBreve, obj.descBreve));
    }

    @Override
    public String toString() {
        return String.format("%s - %s - %s  - Área Atividade: %s", this.descBreve, this.descDetalhada, this.id, this.area.toString());
    }

    public String getDescBreve() {
        return descBreve;
    }

    public void setDescBreve(String descBreve) {
        this.descBreve = descBreve;
    }

    public String getDescDetalhada() {
        return descDetalhada;
    }


    public String getId() {
        return id;
    }


    public AreaAtividade getArea() {
        return area;
    }

}
