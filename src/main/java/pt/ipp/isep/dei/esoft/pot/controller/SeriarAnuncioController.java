package pt.ipp.isep.dei.esoft.pot.controller;

import pt.ipp.isep.dei.esoft.pot.model.*;

import java.util.List;

public class SeriarAnuncioController {

    private final Plataforma plat;
    private final Organizacao org;
    private RegistoColaborador rc;
    private List<Colaborador> listaC;
    private List<Colaborador> listaColParticipantes;
    private RegistoAnuncio la;
    private Anuncio anuncio;

    public SeriarAnuncioController(){
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO)){
            throw new IllegalStateException("Utilizador não Autorizado");
        }
        this.plat=AplicacaoPOT.getInstance().getPlataforma();
        this.org=this.plat.getOrganizacaoByEmail(AplicacaoPOT.getInstance().getSessaoAtual().getEmailUtilizador());
    }
    
    public List<Colaborador> getListaColaboradores(){
        try {
            this.rc=org.getRegistoColaborador();
            this.listaC=rc.getListaColaboradores();
            return this.listaC;
        } catch (Exception e) {
            return null;
        }
    }
    
    public boolean addColParticipantesByID(String idColParticipante, String listaC){
        try {
            return this.listaColParticipantes.add(this.rc.getColaboradorByID(idColParticipante));
        } catch (Exception e) {
            return false;
        }
    }
    
    public String getListaAnuncio(){
        this.la=this.org.getAnuncios();
        return la.toString();
    }
    
    public void getAnuncioByID(String idAnuncio){
        this.anuncio=this.la.getAnuncioByID(idAnuncio);
    }
    
    public void verificacaoAnuncio(){
        this.la.verificaAnuncio(this.anuncio);
    }
    
    public void novaCandidaturasSeriadas(String anuncio){
        this.rcs=la.getRegistoCandidaturasSeriadas();
        this.seriacao=this.rcs.novaCandidaturasSeriadas(this.anuncio, this.listaColParticipantes);
    }
    
    public void seriarListaCandidaturas(String idCandidatura, String lc){
        this.seriacao.addCandidaturaByID(idCandidatura, lc);
        this.seriacao.addClassificacao(idClassificacao, lc, lugar);
    }
    
    public void addData(){
        this.seriacao.addData(data);
    }
    
    public void validaCandidaturasSeriadas(){
        this.rcs.validaCandidaturasSeriadas(this.seriacao);
    }
    
    public void registaCandidaturasSeriadas(){
        this.rcs.validaCandidaturasSeriadas(this.seriacao);
        this.rcs.addCandidaturas(this.seriacao);
    }
}
