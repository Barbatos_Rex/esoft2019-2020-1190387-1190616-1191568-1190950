package pt.ipp.isep.dei.esoft.pot.controller;

import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.*;

import java.util.List;

public class EfetuarCandidaturaController<listaAnunciosDisponiveis> {
    private Plataforma m_oPlataforma;
    private AplicacaoPOT app;
    private SessaoUtilizador sessao;
    private String emailUser;
    private RegistoFreelancer rf;
    private Freelancer m_oFreelancer;
    private List<Freelancer> listaFreelancer;
    private List<Anuncio> listaAnunciosDisponiveis;
    private String id;
    public EfetuarCandidaturaController() {
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
        this.app = AplicacaoPOT.getInstance();
        this.sessao = AplicacaoPOT.getInstance().getSessaoAtual();
        this.emailUser = sessao.getEmailUtilizador();
        this.rf = m_oPlataforma.getRegistoFreelancer();
        this.id=Anuncio.getId();
    }
    public void getFreelancerByEmail (String emailUser) throws Exception {
       m_oFreelancer=RegistoFreelancer.getFreelancerByEmail(emailUser);
    }

    public List<Anuncio> getListaAnunciosDisponiveis() {
        return listaAnunciosDisponiveis;
    }






}