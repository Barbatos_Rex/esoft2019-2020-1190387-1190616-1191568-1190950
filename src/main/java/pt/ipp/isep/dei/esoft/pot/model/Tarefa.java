package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Objects;

public class Tarefa {

    private double dur;
    private String ref;
    private String dsInformal;
    private String dsTecnica;
    private double cust;
    private CategoriaTarefa cat;

    public Tarefa(String ref, String dsInformal, String dsTecnica,double dur, double cust, CategoriaTarefa cat) {
        this.ref = ref;
        this.dsInformal = dsInformal;
        this.dsTecnica = dsTecnica;
        this.dur=dur;
        this.cust = cust;
        this.cat = cat;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getDsInformal() {
        return dsInformal;
    }

    public void setDsInformal(String dsInformal) {
        this.dsInformal = dsInformal;
    }

    public String getDsTecnica() {
        return dsTecnica;
    }

    public void setDsTecnica(String dsTecnica) {
        this.dsTecnica = dsTecnica;
    }

    public double getDur() {
        return dur;
    }

    public void setDur(double dur) {
        this.dur = dur;
    }

    public double getCust() {
        return cust;
    }

    public void setCust(double cust) {
        this.cust = cust;
    }

    public CategoriaTarefa getCat() {
        return cat;
    }

    public void setCat(CategoriaTarefa cat) {
        this.cat = cat;
    }

    @Override
    public String toString() {
        return "Tarefa{" +
                "ref='" + ref + '\'' +
                ", dsInformal='" + dsInformal + '\'' +
                ", dsTecnica='" + dsTecnica + '\'' +
                ", cust=" + cust +
                ", cat=" + cat +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tarefa)) return false;
        Tarefa tarefa = (Tarefa) o;
        return Double.compare(tarefa.cust, cust) == 0 &&
                Objects.equals(ref, tarefa.ref) &&
                Objects.equals(dsInformal, tarefa.dsInformal) &&
                Objects.equals(dsTecnica, tarefa.dsTecnica) &&
                Objects.equals(cat, tarefa.cat);
    }
    @Override
    public int hashCode(){
        int hash = 7;
        hash = 23 * hash + this.ref.hashCode();
        return hash;
    }
}
