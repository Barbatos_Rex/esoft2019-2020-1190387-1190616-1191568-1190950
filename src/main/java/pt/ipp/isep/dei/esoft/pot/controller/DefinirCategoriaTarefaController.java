package pt.ipp.isep.dei.esoft.pot.controller;


import pt.ipp.isep.dei.esoft.pot.model.*;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DefinirCategoriaTarefaController {
    private CompetenciaTecnica m_oCompetencia;
	private final Plataforma m_oPlataforma;
	private CategoriaTarefa m_oCategoriaTarefa;

    public DefinirCategoriaTarefaController() {
        if (!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }

    public List<AreaAtividade> getAreasAtividade() {
        return this.m_oPlataforma.getAreasAtividade();
    }

    public List<CompetenciaTecnica> getCompetenciasTecnicas() {
        return this.m_oPlataforma.getCompetenciasTecnicas();
    }

    public boolean novaCategoriaTarefa(String desc, String aatv, ArrayList<String> compTecn) {
        try {
            AreaAtividade area = this.m_oPlataforma.getAreaAtividadeById(aatv);
            this.m_oCategoriaTarefa = this.m_oPlataforma.novaCategoriaTarefa(desc, aatv, compTecn);
            if (!this.m_oPlataforma.validaCategoriaTarefa(this.m_oCategoriaTarefa)) {
                this.m_oCategoriaTarefa = null;
                return false;
            }
            return true;
        } catch (Exception ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oCompetencia = null;
            return false;
        }
    }

	public boolean RegistoCategoriaTarefa() {
		try {
			return this.m_oPlataforma.RegistoCategoriaTarefa(this.m_oCategoriaTarefa);
		} catch (Exception e) {
			return false;
		}
	}

}
