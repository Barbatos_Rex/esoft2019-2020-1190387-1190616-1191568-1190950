/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.ipp.isep.dei.esoft.pot.model;

import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;

import java.util.*;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Plataforma {
	private final String designacao;
	private final AutorizacaoFacade m_oAutorizacao;
	private final Set<Organizacao> m_lstOrganizacoes;
	private final Set<AreaAtividade> m_lstAreasAtividade;
	private final Set<CompetenciaTecnica> compTec;
	private final Set<CategoriaTarefa> m_lstCategoriasTarefa;
    private final Timer interlavo;
    private RegistoFreelancer freelancers;
	private RegistoAnuncio anuncio;
	private final RegistoOrganizacao organizacoes;
	private final RegistoAdjudicacao adjudicacoes;
    private final Timer delay;

    public Plataforma(String strDesignacao, Timer delay) {
        this.delay = delay;
        if ((strDesignacao == null) ||
				(strDesignacao.isEmpty()))
			throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

		this.designacao = strDesignacao;


		this.m_oAutorizacao = new AutorizacaoFacade();

		this.m_lstOrganizacoes = new HashSet<>();
		this.m_lstAreasAtividade = new HashSet<>();
		this.compTec = new HashSet<>();
		this.m_lstCategoriasTarefa = new HashSet<>();
		this.adjudicacoes = new RegistoAdjudicacao();
		this.organizacoes = new RegistoOrganizacao();
	}
    
    public AutorizacaoFacade getAutorizacaoFacade()
    {
        return this.m_oAutorizacao;
    }
    
    // Organizações
    
    // <editor-fold defaultstate="collapsed">

    public Organizacao novaOrganizacao(String strNome, String strNIF, String strWebsite,String strTelefone, String strEmail, String strpassword, EnderecoPostal oMorada, Colaborador oGestor)
    {
        return new Organizacao(strNome,strNIF, strWebsite, strTelefone, strEmail,oMorada, oGestor,strpassword);
    }

    public boolean registaOrganizacao(Organizacao oOrganizacao, String strPwd)
    {
        if (this.validaOrganizacao(oOrganizacao,strPwd))
        {
            Colaborador oGestor = oOrganizacao.getGestor();
            String strNomeGestor = oGestor.getNome();
            String strEmailGestor = oGestor.getEmail();
            if (this.m_oAutorizacao.registaUtilizadorComPapeis(strNomeGestor,strEmailGestor, strPwd, 
                    new String[] {Constantes.PAPEL_GESTOR_ORGANIZACAO,Constantes.PAPEL_COLABORADOR_ORGANIZACAO}))
                return addOrganizacao(oOrganizacao);
        }
        return false;
    }

    private boolean addOrganizacao(Organizacao oOrganizacao)
    {
        return m_lstOrganizacoes.add(oOrganizacao);
    }
    
    public boolean validaOrganizacao(Organizacao oOrganizacao,String strPwd)
    {
        boolean bRet = true;
        
        // Escrever aqui o código de validação
        if (this.m_oAutorizacao.existeUtilizador(oOrganizacao.getGestor().getEmail()))
            bRet = false;
        if (strPwd == null)
            bRet = false;
        if (strPwd.isEmpty())
            bRet = false;
        //

        return bRet;
    }

    public Organizacao getOrganizacaoByEmail(String emailDoColaborador) {
        for (Organizacao org : this.m_lstOrganizacoes) {
            if (org.getGestor().getEmail().equalsIgnoreCase(emailDoColaborador)) {
                return org;
            }
        }
        return null;
    }

    public Colaborador novoColaborador(String nome, String funcao, String telemovel, String email, String password) {
        m_oAutorizacao.registaUtilizadorComPapel(nome, email, password, funcao);
        return new Colaborador(nome, funcao, telemovel, email, password);
    }
    // </editor-fold>


    // Competências Tecnicas

    // <editor-fold defaultstate="collapsed">
    
    public CompetenciaTecnica getCompetenciaTecnicaById(String strId) {
        for (CompetenciaTecnica oCompTecnica : this.compTec) {
            if (oCompTecnica.hasId(strId)) {
                return oCompTecnica;
            }
        }

        return null;
    }

    public CompetenciaTecnica novaCompetenciaTecnica(String descBreve, String descDetalhada, String id, String idArea) {
        return new CompetenciaTecnica(descBreve, descDetalhada, id, getAreaAtividadeById(idArea));
    }

    public boolean registaCompetenciaTecnica(CompetenciaTecnica oCompTecnica)
    {
        if (this.verificarCompetenciaTecnica(oCompTecnica)) {
            return addCompetenciaTecnica(oCompTecnica);
        }
        return false;
    }

    private boolean addCompetenciaTecnica(CompetenciaTecnica competencia) {
        return compTec.add(competencia);
    }

    public boolean verificarCompetenciaTecnica(CompetenciaTecnica oCompTecnica) {
        if (oCompTecnica.getDescBreve() == null) {
            return false;
        }
        if (oCompTecnica.getArea() == null) {
            return false;
        }
        if (oCompTecnica.getDescDetalhada() == null) {
            return false;
        }
        if (oCompTecnica.getId() == null) {
            return false;
        }
        for (CompetenciaTecnica comp : compTec) {
            if (comp.equals(oCompTecnica)) {
                return false;
            }
        }
        return true;
    }

    // </editor-fold>

    // Areas de Atividade 
    // <editor-fold defaultstate="collapsed">
            
    public AreaAtividade getAreaAtividadeById(String strId)
    {
        for(AreaAtividade area : this.m_lstAreasAtividade)
        {
            if (area.hasId(strId))
            {
                return area;
            }
        }
        
        return null;
    }

    public AreaAtividade novaAreaAtividade(String strCodigo, String strDescricaoBreve, String strDescricaoDetalhada)
    {
        return new AreaAtividade(strCodigo, strDescricaoBreve,strDescricaoDetalhada);
    }

    public boolean registaAreaAtividade(AreaAtividade oArea)
    {
        if (this.validaAreaAtividade(oArea))
        {
            return addAreaAtividade(oArea);
        }
        return false;
    }

    private boolean addAreaAtividade(AreaAtividade oArea)
    {
        return m_lstAreasAtividade.add(oArea);
    }
    
    public boolean validaAreaAtividade(AreaAtividade oArea)
    {
        boolean bRet = true;
        
        // Escrever aqui o código de validação
        
        //
      
        return bRet;
    }
    
    public List<AreaAtividade> getAreasAtividade()
    {
        List<AreaAtividade> lc = new ArrayList<>();
        lc.addAll(this.m_lstAreasAtividade);
        return lc;
    }
    public List <CompetenciaTecnica> getCompetenciasTecnicas(){
        List<CompetenciaTecnica> lct = new ArrayList<>();
        lct.addAll(this.compTec);
        return lct;
    }

    public List<CategoriaTarefa> getCategorias() {
        List<CategoriaTarefa> lct = new ArrayList<>();
        lct.addAll(this.m_lstCategoriasTarefa);
        return lct;
    }

    // </editor-fold>

    // Categoria de Tarefa
    public CategoriaTarefa novaCategoriaTarefa(String desc, String aatv, ArrayList<String> compTec) {
        ArrayList<CompetenciaTecnica> tmp = new ArrayList<>();
        for (String s : compTec) {
            for (CompetenciaTecnica c : this.compTec) {
                if (s.equalsIgnoreCase(c.getId())) {
                    tmp.add(c);
                }
            }
        }
        return new CategoriaTarefa(desc, this.getAreaAtividadeById(aatv), tmp);
    }

    public boolean validaCategoriaTarefa(CategoriaTarefa ct) {
        return ct.getDescricao() != null && !ct.getDescricao().isEmpty() && ct.getAatv() != null && ct.getComptecn() != null && ct.getComptecn().size() >= 1;
    }

    public boolean RegistoCategoriaTarefa(CategoriaTarefa ct) {
        if (this.validaCategoriaTarefa(ct)) {
            return this.m_lstCategoriasTarefa.add(ct);
        }
        return false;
    }

    public CategoriaTarefa getCategoriasByRef(String catByRef) {
        for (CategoriaTarefa ct : this.m_lstCategoriasTarefa) {
            if (ct.getId().equalsIgnoreCase(catByRef)) {
                return ct;
            }
        }
        return null;
    }


    public RegistoFreelancer getRegistoFreelancer() {

        return this.freelancers;
    }
    public RegistoAnuncio getRegistoAnuncio(){

        return this.anuncio;
    }

	public boolean addFreelancer(Freelancer freelancer) {
		if (freelancer.validaFreelancer()) {
			return this.freelancers.addFreelancer(freelancer);
		}

		return false;
	}

	public RegistoOrganizacao getRegistoOrganizacao() {
		return this.organizacoes;
	}

	public RegistoAdjudicacao getRegistoAdjudicacao() {
		return this.adjudicacoes;
	}
	public Timer getDealay(){return this.delay; }
	public  Timer getInterval(){return this.interlavo;}
	public Timer agendaSeriacao(){}

}
    
    
