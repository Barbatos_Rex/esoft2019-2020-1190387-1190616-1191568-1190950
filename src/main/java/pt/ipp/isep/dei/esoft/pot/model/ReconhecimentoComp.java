package pt.ipp.isep.dei.esoft.pot.model;

public class ReconhecimentoComp {
    private String data;
    private CompetenciaTecnica compTec;
    private GrauProficiencia grauProf;

    public ReconhecimentoComp(String data,CompetenciaTecnica compTec,GrauProficiencia grauProf) {
        if ((data == null) || (compTec == null) || (grauProf == null)  || (data.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        this.data = data;
        this.compTec = compTec;
        this.grauProf = grauProf;
    }
}
