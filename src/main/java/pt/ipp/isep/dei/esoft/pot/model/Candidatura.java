package pt.ipp.isep.dei.esoft.pot.model;

public class Candidatura {



    public String getCodigoFreelancer;
    String id;
    private int valor;

    private int nrDias;
    private String txtApres;
    private String txtMotiv;
    private final String texto;
    private final String TEXTO_POR_OMISSAO = "Não introduzido";
    private String anuncioId;
    private double valorPrt;
    private Freelancer freelancer;

    public String getGetCodigoFreelancer() {
        return getCodigoFreelancer;
    }

    public void setGetCodigoFreelancer(String getCodigoFreelancer) {
        this.getCodigoFreelancer = getCodigoFreelancer;
    }

    public int getValor() {
        return valor;
    }

    public int getNrDias() {
        return nrDias;
    }

    public String getTexto() {
        return texto;
    }

    public Candidatura(int valor, int nrDias, String texto) {
        this.nrDias = nrDias;
        this.valor = valor;
        this.texto = texto;
    }

    public void atualizarCandidatura(double valorPrt, int nrDias, String txtApres, String txtMotiv) {
        this.valorPrt = valorPrt;
        this.nrDias = nrDias;
        this.txtApres = txtApres;
        this.txtMotiv = txtMotiv;
    }


    public String getId() {

        return id;
    }
        public Freelancer getFreelancer () {
            return this.freelancer;
        }
    }

