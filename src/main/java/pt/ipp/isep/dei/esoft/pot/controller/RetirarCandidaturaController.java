package pt.ipp.isep.dei.esoft.pot.controller;

import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncio;

import java.util.ArrayList;
import java.util.List;

public class RetirarCandidaturaController {
    private final Plataforma plat;
    private RegistoAnuncio ra;
    private Candidatura candidatura;

    public RetirarCandidaturaController() {
        this.plat = AplicacaoPOT.getInstance().getPlataforma();
    }

    public List<String> getListaCandidaturas() {
        List<Candidatura> lc = new ArrayList<>();
        List<String> laString = new ArrayList<>();
        this.ra = plat.getRegistoAnuncio();
        for (Anuncio a : ra.getAnuncios()) {
            for (Candidatura c : a.getCandidaturas()) {
                if (c.getFreelancer().getEmail().equals(AplicacaoPOT.getInstance().getSessaoAtual().getEmailUtilizador())) {
                    lc.add(c);
                }
            }
        }
        return listToListString(lc);
    }

    private List<String> listToListString(List<Candidatura> lc) {
        List<String> ls = new ArrayList<>();
        for (Candidatura c : lc) {
            ls.add(lc.toString());
        }
        return ls;
    }

    public String get(String idAnuncio) {
        for (Anuncio a : ra.getAnuncios()) {
            if (idAnuncio.equals(a.getId())) {
                for (Candidatura c : a.getCandidaturas()) {
                    if (c.getFreelancer().getEmail().equals(AplicacaoPOT.getInstance().getSessaoAtual().getEmailUtilizador())) {
                        this.candidatura = c;
                        return c.toString();
                    }
                }
            }
        }
        return null;
    }

    public boolean removeCandidatura(String candidatura) {
        for (Anuncio a : ra.getAnuncios()) {
            a.getCandidaturas().remove(this.candidatura);
        }
        return true;
    }

}
