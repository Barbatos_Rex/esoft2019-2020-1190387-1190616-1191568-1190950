package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Date;

public class Adjudicacao {

	private final String refAnuncio;
	private final String refOrg;
	private final Freelancer freelancer;
	private final Date dataAfetoInicial;
	private final Date dataAfetoFinal;
	private final double valorAPagar;
	private final String descTarefa;

	public Adjudicacao(String refOrg, Freelancer freelancer, String descTarefa, Date dataAfetoInicial, Date dataAfetoFinal, double valorAPagar, String refAnuncio) {
		if (refOrg.isEmpty() || refOrg.isEmpty() || descTarefa.isEmpty() || refAnuncio.isEmpty() || refOrg == null || freelancer == null || descTarefa == null || dataAfetoInicial == null || dataAfetoFinal == null || refAnuncio == null) {
			throw new IllegalArgumentException("Nenhum parâmetro pode ser nulo ou vazio!");
		}
		this.refAnuncio = refAnuncio;
		this.refOrg = refOrg;
		this.freelancer = freelancer;
		this.dataAfetoInicial = dataAfetoInicial;
		this.dataAfetoFinal = dataAfetoFinal;
		this.valorAPagar = valorAPagar;
		this.descTarefa = descTarefa;
	}

	public boolean verificaAdjudicacao(Adjudicacao adj) {
		return !adj.refOrg.isEmpty() && !adj.refOrg.isEmpty() && !adj.descTarefa.isEmpty() && !adj.refAnuncio.isEmpty() && adj.refOrg != null && adj.freelancer != null && adj.descTarefa != null && adj.dataAfetoInicial != null && adj.dataAfetoFinal != null && adj.refAnuncio != null;
	}

}
