package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;

public class ListaCandidaturas {

    List<Candidatura>candidaturaList;
    public ListaCandidaturas(){
        candidaturaList= new ArrayList<>();
    }

	private final List<Candidatura> candidaturas = new ArrayList<>();
    public Candidatura getCandidaturaById(String candId) {
        for(Candidatura c: candidaturaList){
            if(c.getId().equals(candId)){
                return c;
            }
        }
        return null;
    }

    public List<Candidatura> getCandidaturasDoFreelancer(Freelancer freel) {
        List<Candidatura> listaCandidaturasDoFreelancer= new ArrayList<>();
        for(Candidatura c: candidaturaList){
            if(c.getCodigoFreelancer.equals(freel.getCodigo())){
                listaCandidaturasDoFreelancer.add(c);
            }
        }
        return listaCandidaturasDoFreelancer;
    }

    public void atualizarCandidatura(Candidatura candidaturaSelecionada, double valorPrt, int nrDias, String txtApres, String txtMotiv) {
    if(validaCandidatura(candidaturaSelecionada)) {
        candidaturaSelecionada.atualizarCandidatura(valorPrt, nrDias, txtApres, txtMotiv);
    }else{
        System.out.println("Falha ao atualizar candidatura.");
    }
    }

    private boolean validaCandidatura(Candidatura candidaturaSelecionada){
        return (candidaturaSelecionada.getValor()!=0 &&candidaturaSelecionada.getNrDias()!=0&& candidaturaSelecionada.getTexto().equals(""));
    }
    public boolean addCandidatura(Candidatura cand){
        return candidaturaList.add(cand);
    }

	public Candidatura getCandidaturaNaPosicao(int posicao) {
		//Por não ter de refinar o código da UC10 Assumo que a ordenação é feita na lista e a melhor seja a primeira
		return candidaturas.get(posicao);
	}
}
