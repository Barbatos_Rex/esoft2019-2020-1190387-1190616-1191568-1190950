package pt.ipp.isep.dei.esoft.pot.model;

import java.util.List;

public class RegistoTarefa {
	private List<Tarefa> tarefas;

	public boolean addTarefa(Tarefa tar) {
		return tarefas.add(tar);
	}

	//<editor-fold desc="UC8">
	public Tarefa getTarefaById(String refTar) throws Exception {
		for (Tarefa t : tarefas) {
			if (t.getRef().equals(refTar)) {
				return t;
			}
		}
		throw new Exception("Nenhuma tarefa com essa referência existe!");
	}
	//</editor-fold>
	//adicionar mais métodos conforme seja necessário!

}
