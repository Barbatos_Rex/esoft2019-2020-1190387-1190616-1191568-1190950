package pt.ipp.isep.dei.esoft.pot.model;

import java.util.List;

public class Freelancer {
    private String codigo;
    private String nome;
    private String telefone;
    private String nif;
    private String email;
    private EnderecoPostal endereco;
    private List<ReconhecimentoComp> rcList;
    private List<HabilitacaoAcad> haList;

    public static EnderecoPostal novoEnderecoPostal(String strLocal, String strCodPostal, String strLocalidade) {
        return new EnderecoPostal(strLocal, strCodPostal, strLocalidade);
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    public Freelancer(String nome,String nif,EnderecoPostal endereco,String telefone,String email){
        if ((nome == null) || (nif == null) || (telefone == null) ||(email == null) ||
                (endereco == null) || (nome.isEmpty()) || (nif.isEmpty()) || (telefone.isEmpty())|| (email.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

        this.nome = nome;
        this.nif = nif;
        this.endereco = endereco;
        this.telefone = telefone;
        this.email = email;
    }
    public void addHabitacaoAcad(HabilitacaoAcad ha){
        haList.add(ha);
    }
    public void addReconhecimentoComp(ReconhecimentoComp rc){
        rcList.add(rc);
    }

    public boolean validaFreelancer() {
        return nome != null && telefone != null && nif != null && email != null && endereco != null && rcList != null && haList != null;

    }

    public String getCodigo() {
        return codigo;
    }
}
