package pt.ipp.isep.dei.esoft.pot.controller;

import pt.ipp.isep.dei.esoft.pot.model.AlgoritmoGeraPwd;
import pt.ipp.isep.dei.esoft.pot.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.model.RegistoFreelancer;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

import java.util.logging.Level;
import java.util.logging.Logger;

public class RegistarFreelancerController implements AlgoritmoGeraPwd {

	private final AplicacaoPOT m_oApp;
	private RegistoFreelancer registoFreelancer;
	private Freelancer freelancer;
	private String m_strPwd;

	public RegistarFreelancerController() {
		this.m_oApp = AplicacaoPOT.getInstance();
	}

	public boolean novoFreelancer(String nome, String nif, String strLocal, String strCodPostal, String strLocalidade, String telefone, String email) {
		try {
			this.m_strPwd = geraPwd(nome, email);
			EnderecoPostal oMorada = Freelancer.novoEnderecoPostal(strLocal, strCodPostal, strLocalidade);
			this.freelancer = new Freelancer(nome, nif, oMorada, telefone, email);
			return this.freelancer.validaFreelancer();
		} catch (RuntimeException ex) {
			Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.freelancer = null;
            return false;
        }
    }

    @Override
    public String geraPwd(String nome, String email) {
        return "pwd";
    }

    public boolean registaFreelancer(){
        return this.registoFreelancer.registaFreelancer(this.freelancer);
    }

}
