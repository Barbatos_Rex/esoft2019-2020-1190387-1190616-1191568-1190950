package pt.ipp.isep.dei.esoft.pot.model;


import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class RegistoFreelancer {
    private List<Freelancer>   freelancerList;

    public RegistoFreelancer() {
       this.freelancerList= new ArrayList<>();
    }


    public boolean registaFreelancer(Freelancer freelancer){

        return addFreelancer(freelancer);
    }
    public boolean addFreelancer(Freelancer freelancer) {
        return freelancerList.add(freelancer);
    }

    public static Freelancer getFreelancerByEmail(String email) throws Exception {
       for (Freelancer free : freelancerList) {
            if (free.getEmail().equals(email)) {
                return free;
            }
        }
        throw new Exception("Não existe com um Freelancer com o email introduzido!");
    }

    public List<Freelancer> getfreelancerList() {
        return freelancerList;
    }


    public Freelancer  novoFreelancer(String nome,String nif,EnderecoPostal endereco,String telefone,String email){
        return new Freelancer(nome,nif,endereco,telefone,email);
    }

    public boolean validaFreelancer(Freelancer freelancer)
    {
        return freelancer.validaFreelancer();
    }
}
