package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;

public class RegistoOrganizacao {
	private final List<Organizacao> organizacoes;

	public RegistoOrganizacao() {
		this.organizacoes = new ArrayList<>();
	}

	public Organizacao getOrganizacaoByEmail(String emailCol) {
		for (Organizacao org : organizacoes) {
			if (org.getGestor().getEmail().equals(emailCol)) {
				return org;
			}
		}
		return null;
	}
}
