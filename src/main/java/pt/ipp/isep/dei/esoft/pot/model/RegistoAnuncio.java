package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;

public class RegistoAnuncio {
	private static List<Anuncio> anuncios;

	public RegistoAnuncio() {
		anuncios = new ArrayList<>();
	}

	public boolean addAnuncio(Anuncio anun) {
		return anuncios.add(anun);
	}

	public boolean verificaAnuncio(Anuncio anun) {
		return anun.verificarAnuncio();
	}

	public List<Anuncio> getAnuncios() {
		return anuncios;
	}

	public static List<Anuncio> getAnunciosDisponivies() {
		Object anunciosDisponivies = null;
//        if()
		return (List<Anuncio>) anunciosDisponivies;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Anuncio a : anuncios) {
			sb.append(a + "\n");
		}
		return sb.toString();
	}

	public Anuncio getAnuncioById(String ref) {
		for (Anuncio a : anuncios) {
			if (a.getId().equals(ref)) {
				return a;
			}
		}
		return null;
	}
}
