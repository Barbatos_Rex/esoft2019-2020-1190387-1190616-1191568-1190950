package pt.ipp.isep.dei.esoft.pot.model;

public class GrauProficiencia {
    private static String grauProf;
    public GrauProficiencia(String grauProf){
        if ((grauProf == null) || (grauProf.isEmpty()))
            throw new IllegalArgumentException("O argumento não pode ser nulo ou vazio.");
        this.grauProf=grauProf;
    }

    public static String getGrauProf() {
        return grauProf;
    }
}
