package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;

public class RegistoColaborador {
    private List<Colaborador>colaboradores;

    public RegistoColaborador() {
        this.colaboradores=new ArrayList<>();
    }

    public List<Colaborador>getListaColaboradores() {
        return colaboradores;
    }

    public Colaborador getColaboradorByID(String idColParticipante) {
        for(Colaborador c:colaboradores){
            if(c.getEmail().equals(idColParticipante)){
                return c;
            }
        }
        return null;
    }
}
