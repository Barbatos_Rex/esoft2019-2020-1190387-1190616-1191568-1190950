package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.List;

public class CategoriaTarefa {
    private String id;
    private String descricao;
    private AreaAtividade aatv;
    private List<CompetenciaTecnica> comptecn = new ArrayList<>();

    public CategoriaTarefa(String desc, AreaAtividade aatv, List<CompetenciaTecnica> comptecn) {
        if ((id == null) || (id.isEmpty()) || (desc == null) || (aatv == null) || (comptecn == null) || (desc.isEmpty()) || (comptecn.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        this.descricao = desc;
        this.aatv = aatv;
        this.comptecn = comptecn;
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public AreaAtividade getAatv() {
        return aatv;
    }


    public List<CompetenciaTecnica> getComptecn() {
        return comptecn;
    }

    public String getId() {
        return this.id;
    }


}
