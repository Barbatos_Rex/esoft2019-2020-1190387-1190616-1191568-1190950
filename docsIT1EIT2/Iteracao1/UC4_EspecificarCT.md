# UC 4 - Especificar Competência Técnica
## 1. Engenharia de requesitos

### Formato breve
O Administrativo faz a abertura de uma nova competência técnica. Para tal, devem ser introduzidos um código único e duas descrições sobre essa competência. No final o sistema gurda a tal competência técnica.

### SSD
![Esquema_UC4](UC4_EspecificarCT.svg)

### Formato completo
O Administrativo faz a abertura de uma nova competência técnica.

#### Ator principal
O Administrativo (ADM)

#### Partes intressadas e seus interesses
* **Administrativo:** pretende criar uma nova competência técnica.
* **_Freelancer_:** vais poder saber em concreto as competências necessárias para uma tarefa.
* **TJ4:** pretende que a plataforma permita catalogar as competências técnicas e as categorias de tarefas em áreas de atividade. 


#### Pré-condições
Uma área de atividade ([Ver UC2](UC2_DefinirArea.md))

#### Pos-condições
A competência técnica é guardada no Sistema.

#### Cenários de sucesso principal (ou fluxo básico)

1. O ADM faz o pedido de criação de uma nova competência técnica cujo objetivo é complementar uma tarefa ([Ver UC3](UC3_DefinirCategoria.md)).
2. O Sistema pede como parâmetros um código único, uma descrição breve e uma detalhada.
3. O ADM introduz os valores para os tais parâmetros pedidos pelo Sistema.
4. O Sistema lista todas as áreas de atividade e pede uma área de atividade.
5. O ADM introduz o parâmetro pedido pelo Sistema.
6. O Sistema recebe o parametro e faz a criação da competencia.
7. O Sistema mostra o resultado e pede confirmação.
8. O ADM confirma a criação.
9. O Sistema retorna "Operação bem sucedida".


#### Extensões (ou fluxo secundário)
*a O Administrartivo cancela o pedido de criação de uma competência técnica
>O caso de uso termina

3a O Administrativo não introduz um código válido
>1. O sistema informa da invalidade do código
>2. O sistema pede ao Administrativo que introduza um código válido.
>
    > 2a O Administrativo não introduz um código válido. O Caso de uso termina.

3b O Administrativo não introduz a descrição breve
>1. O sistema informa da falta dessa descrição
>2. O sistema pede ao Administrativo que escreva a descrição.
   
    > 2a O Administrativo não produz a descrição. O caso de uso termina.

3c O Adiministrativo não introduz a descrição mais longa
>1. O sistema informa da falta dessa descrição
 >2. O sistema pede ao Administrativo que escreva a descrição.
    
     > 2a O Administrativo não produz a descrição. O caso de uso termina.


5a O Administrativo não introduz uma área de atividade válida.
>1. O sistema informa da invalidade da área de atividade.
>2. O sistema pede ao Administrativo que introduza uma área de atividade válida.

    > O Administrativo não introduz uma área de atividade válida. O caso de uso termina.


#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* O código único é sempre introduzido pelo administrativo ou o sistema deve gerá-lo automaticamente?
* As descrições têm limite máximo de caractéres? E mínimo?
* Qual a frequência de ocorrência deste caso de uso?

## Análise de Domínio (AOO)

![Esquema AOO](UC4_MD.svg)

## 3. Design - Realização do Caso de Uso
### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| |	... interage com o utilizador? | DefinirCompetenciaTecnicaUI    |  Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
| 1. O ADM faz o pedido de criação de uma nova competência técnica cujo objetivo é complementar uma tarefa. 		 |	... coordena o UC?	| DefinirCompetenciaTecnicaController | Controller    |
|  		 |	... cria instância de CompetenciaTecnica?| Plataforma   | Creator (Regra1)   |
||
| 2.O Sistema pede como parâmetros um código único, uma descrição breve e outra detalhada. | 							 |             |                              |
||
| 3. O ADM introduz os valores para os tais parâmetros pedidos pelo Sistema.  		 |	... guarda os dados introduzidos temporariamente?  |   DefinirCompetenciaTecnicaController | Controller    |
||
| 4. O Sistema lista as áreas de atividade e pede uma à qual a competência técnica é necessária.|	...tem a lista de áreas de atividade?						 |     Plataforma        |         Creator(Regra 3)                    |		 
||
| |	... guarda todos os dados introduzidos? | CompetenciaTecnica | Information Expert (IE) - instância criada no passo 1   |					 
|5. O ADM introduz o parâmetro pedido pelo Sistema. || 
|       |... representa os dados pedidos? | AreaDeAtividade|  Information Expert (IE) - instância criada previamente [(Ver UC2)](UC2_DefinirArea.md)|
||
|6. O Sistema recebe o parametro e faz a criação da competencia.| ...cria a competencia técnica?| Plataforma| Creator(Reagra 1)|
||
|7. O Sistema mostra o resultado e pede confirmação.|
||
|8. O ADM confirma a criação.|... valida os dados?| CompetenciaTecnica| IE - Têm os dados necessários para verificar.
| |...guarda a nova competencia?| Plataforma | Creator (Regra 2).
||
|9. O Sistema retorna "Operação bem sucedida".|

### Sistematização

Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * CategoriaTarefa


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * DefinirCategoriaUI  
 * DefinirCategoriaController
 
## Diagrama de Sequência
 
 ![Diagrama de sequencia](UC4_DS.svg)
 
## Diagrama de Classes
 
 ![DC](UC4_CD.svg)
 

