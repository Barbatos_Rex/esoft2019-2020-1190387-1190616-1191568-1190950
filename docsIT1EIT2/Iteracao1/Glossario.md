# Glossário

**Os termos devem estar organizados alfabeticamente.**
~~~~(Completar)

| **_Termo_**                   	| **_Descrição_**                                           |                                       
|:------------------------|:----------------------------------------------------------------|
| **Administrativo** | Pessoa responsável por realizar na plataforma várias atividades de suporte ao negócio.|
| **ADM** | Acrónimo para Administrativo.|
| **Categoria (de Tarefa)** | Corresponde a uma descrição usada para catalogar um ou mais tarefas (semelhantes).|
| **Organização** | Pessoa coletiva que pretende contratar _freelancers_ para a realização de tarefas necessárias à atividade da mesma.|
| **_Freelancer_** | Pessoa individual que trabalha por conta própria e não está necessariamente comprometida com uma entidade empregadora (organização) específica a longo prazo.|
|**_Outsiurcing_**|Pessoa externa que trabalha para a organização, não estando necessariamente comprometida com uma organização  |
| **Processo de Autenticação** | Meio através do qual se procede à verificação da identidade da pessoa que pretende/está a utilizar a plataforma informática.|
| **Utilizador** | Pessoa que interage com a aplicação informática.|
| **Utilizador Não Registado** | Utilizador que interage com a plataforma informática de forma anónima, i.e. sem ter realizado previamente o processo de autenticação previsto.|
| **Utilizador Registado** | Utilizador que interage com a plataforma informática após ter realizado o processo de autenticação previsto e, portanto, a aplicação conhece a sua identidade. Tipicamente, este assume o papel/função de Administrativo ou Gestor de Organização ou Colaborador de Organização ou _Freelancer_.|
| **Competência Técnica** | Habilidade necessária para realizar uma dada tarefa proposta pelo ADM| 
| **Colaborador da Organização** | pessoa registada na plataforma como sendo alguém que atua em representação de uma determinada organização. Entre outras responsabilidades, cabe-lhe especificar tarefas para posterior publicação pela organização respetiva.|
| **Competência Técnica** | Habilidade necessária para realizar uma dada tarefa proposta pelo ADM| 
| **Gestor Da Organização** | Pessoa indicada como gestor da organização aquando do resgisto da organização na plataforma, sendo responsavel por especificar na plataforma outros colaboradores dessa mesma organização.|
| **T4J** | Abreviação de **_Tasks For Joe_**, a compania que fornece a utilização da plataforma plataforma|
|:---------------------------------------------------------------------------------------|







