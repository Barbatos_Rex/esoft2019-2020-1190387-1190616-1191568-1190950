#UC3 - Definir Categoria de Tarefa
## 1. Engenharia de Requisitos

### Formato Breve

O Administrativo inicia a definição de uma nova categoria de tarefa. O sistema solicita os dados requeridos( i.e. descrição, área de atividade e lista de competências técnicas). O administrativo introduz os dados solicitados. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. O administrativo confirma. O sistema regista os dados e informa o administrativo do sucesso da operação.

### SSD
![UC3_SSD.svg](UC3_SSD.svg)


### Formato Completo

#### Ator principal

Administrativo

#### Partes interessadas e seus interesses
* **Administrativo:** pretende definir a categoria de tarefa.
* **T4J:** pretende que a plataforma permita catalogar as categorias de tarefas em áreas de atividade.


#### Pré-condições
Ter registado o utilizador como administrador anteriormente.

#### Pós-condições
A informação da categoria de tarefa é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia a definição de uma nova categoria de tarefa. 
2. O sistema solicita os dados necessários (i.e. c descrição). 
3. O administrativo introduz os dados solicitados(descrição).
4. O sistema apresenta lista de Áreas de Atividade.
5. O administrativa escolhe uma Área de Atividade.
6. O sistema mostra lista de Competências Técnicas.
7. O administrativo escolhe uma Competência Técnica.
8. O sistema solicita indicação de obrigatoriedade(S/N).
9. O administrativo introduz a informação da obrigatoriedade.
10. Os pontos 7 a 9 repetem-se até o administrativa não querer adicionar mais categorias de tarefa.
11. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. 
12. O administrativo confirma.
13. O sistema regista os dados e informa o administrativo do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O administrativo solicita o cancelamento da definição da categoria de tarefa.

> O caso de uso termina.
>
4a. O sistema deteta que a lista de Área de Atividade se encontra vazia.
>   1. O sistema informa que a lista de Área de Atividde está vazia.
>   >2.O caso de uso termina.
>
6a. O sistema deteta que a lista que a lista de Competências Técnicas se encontra vazia.
 >   1. O sistema informa que a lista de Competências Técnicas está vazia.
 >   >2.O caso de uso termina.
 >
7a. O sistema deteta que o administrativo não seleciona qualquer competência Técnica.
 >   1. O sistema informa que o administrativo não seleciona qualquer competência Técnica.
 >   2. O caso de uso termina.
>
	>	2a. O sistema apresenta uma competência técnica obrigatória. 
11a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

11b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o administrativo para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

11c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o administrativo para o facto. 
> 2. O sistema permite a sua alteração (passo 3).
> 
	> 2a. O administrativo não altera os dados. O caso de uso termina. 

#### Requisitos especiais
\- 

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* O código único é sempre introduzido pelo administrativo ou o sistema deve gerá-lo automaticamente?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC3_MD.svg](UC3_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O administrativo inicia a definição de uma nova categoria de tarefa.   		 |	... interage com o utilizador? | DefinirCategoriaUI    |  Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
|  		 |	... coordena o UC?	| DefinirCategoriaController | Controller    |
|  		 |	... cria instância de CategoriaTarefa| Plataforma   | Creator (Regra1)   |
| 2. O sistema solicita os dados necessários (i.e. descrição breve e área de atividade).  		 |							 |             |                              |
| 3. O administrativo introduz os dados solicitados.  		 |	... guarda os dados introduzidos?  |   CategoriaTarefa | Information Expert (IE) - instância criada no passo 1     |
| 4. O sistema apresenta lista de Áreas de Atividade. 		 |							 |             |                              |
| 5. O administrativo escolhe uma Área de Atividade. 		 |							 |             |                              |
| 6. O sistema mostra lista de Competências Técnicas. 		 |							 |             |                              |
| 7. O administrativo escolhe uma Competência Técnica. 		 |							 |             |                              |
| 8. O sistema solicita indicação de obrigatoriedade(S/N).  		 |							 |             |                              |
| 9. O administrativo introduz a informação da obrigatoriedade.  		 |							 |             |                              |
| 10. Os pontos 7 a 9 repetem-se até o administrativo não querer adicionar mais Competências Técnicas.  		 |							 |             |                              |
| 11. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.   		 |	...valida os dados da Categoria (validação local) | CategoriaTarefa |                              |IE. Possui os seu próprios dados.|  	
|	 |	...valida os dados da Categoria (validação global) | Plataforma  | IE: A Plataforma possui/agrega CategoriaTarefa  |
| 12. O administrativo confirma.   		 |							 |             |                              |
| 13. O sistema regista os dados e informa o administrativo do sucesso da operação.  		 |	... guarda a CategoriaTarefa criada? | Plataforma  | IE: No MD a Plataforma possui CategoriaTarefa|  
             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * CategoriaTarefa


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * DefinirCategoriaUI  
 * DefinirCategoriaController


###	Diagrama de Sequência

![UC3_SD.svg](UC3_SD.svg)


###	Diagrama de Classes

![UC3_CD.svg](UC3_CD.svg)




