# UC5 - Especificar Colaborador

## 1. Engenharia de Requisitos

### Formato Breve
O Gestor da Organização inicia o registo na plataforma dos colaboradores. De seguida, o sistema pede os dados necessarios(nome,funcao,telemovel,email,password) para criar os calaboradores

### SSD

![UC5_SSD.svg](UC5_SSD.svg)

### Formato Completo

#### Ator principal
*Utilizador Registado* (Geestor de Organização)

#### Partes interessadas e seus interesses

* **Gestor da organização**-Registo outros colaboradores da organização   para que estes possam desempenhar os seus cargos.
* **T4J**- Prente qie o gestor de organização utilize a platafforma para regitar novos colaboradores

#### Pré-condições
A pessoa tem que estar registada na plataforma como sendo alguém
que atua em representação de uma determinada organização.

#### Pós-condições
A informação do registo ser guardada.

#### Cenário de sucesso principal (ou fluxo básico)
1.  O gestor da organização regita outros colaboradores 
2.  O sistema solicita os dados necessarios sobre esse colaborador (nome,funcao,telemovel,email,password)
3.  O gestor introduz os dados pedidos
4.  O sistema valida-os e pede a sua confirmação
5.  O gestor confirma-os.
6.  O sistema guarda as informações paresentado que a operação foi bem sucedida.

#### Extensões (ou fluxos alternativos)
* a. O gestor de organazição solicita o cancelamentoa do registo.
 >"O caso de uso termina"

* 4a.Dados de Função incompletos.
> 1.O sistema informa quais os dados em falta
>
> 2.O sistema permitea intordução dos em falta(passo3)
>>* 2a. O gestor  da organização nao corrige e o caso de uso termina.
>
* 4b.Dados minimos obrigatorios minimosem falta.
> 1.O sistema informa quais os dados em falta.
>
>> 2.O sistema permite a introdução dos dados em falta(passo 3)
>>* 2a.O gestor de Organização nao introduz e entao o caso de uso termina.
* 4c.O sistema deteta que há dados introduzidos ja existentes.
> 1.O sistema informa quais são os dados que ja existem.
> 2.O sistema permite a sua alteração (parte 3).
>>* 2a. O gestor  da organização nao corrige e o caso de uso termina. 
#### Requisitos especiais
Ser registado e  ter função de gestor da organização
#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
\-Ha mais daods obrigatorios?
\-Existe alguma medida de segurança apos o novo registo do colaborador
## 2. Análise OO
![UC5_MD.svg](UC5_MD.svg)
### Excerto do Modelo de Domínio Relevante para o UC

## 3. Design - Realização do Caso de Uso

### Racional
| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
|1.O Gestor de organização inicia os registo dos colaboradores  		 |	... interage com o gestor de organização	| RegistoColaboradorUI | Pure Fabrication   |
| |...coordena o UC?|RegistoColaboradorController|Controller|
| 2.O sistema solicita os dados necessarios sobre o colabordor||            |                              |
| 3.  O gestor introduz os dados pedidos	 |	... guarda os dados introduzidos?  |   Colaborador | Information Expert (IE) - instância criada no passo 1     |
| 4.  O sistema valida-os e pede a sua confirmação 		 |... valida os dados do Colaborador de organização(validação local)						 | Colaborador|Ex: possui os seus prorpios dados        |
| |..valida os dados|Organização|IE:A organização tem registados Colaboradores
| 5. O gestor confirma-os.  		 |							 |             |                              |
| 6. O sistema guarda as informações paresentado que a operação foi bem sucedida.|...guarda o utillizador referente ao Colaborador|AutorizacaoFAacade|Ex:A gestão de utilizadores é através da Classe "AutorizaçãoFacade"

### Sistematização ##

###	Diagrama de Sequência
![UC5_SD.svg](UC5_SD.svg)

###	Diagrama de Classes
![UC5_CD.svg](UC5_CD.svg)



