# UC6 - Especificar Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

Os colaborador de uma organização inicia a especificação de uma nova tarefa, caracterizada por ter uma referência única por organização, uma designação, uma descrição informal e outra de carácter técnico, uma estimativa de duração e custo, bem como a categoria em que a mesma se enquadra.  

### SSD

![UC6_SSD.svg](UC6_SSD.svg)

### Formato Completo

O colaborador da organização inicia a especificação de tarefa.

#### Ator principal

Os colaboradores da organização.

#### Partes interessadas e seus interesses
* **Colaborador da organização:** tem como função experimentar a tarefa inicializadas pelo mesmo.
* **T4J:** pretende que a plataforma permita catalogar as competências técnicas e as categorias de tarefas em áreas de atividade.
* **_Freelancer_:** pessoa individual que trabalha por conta própria e não está necessariamente comprometida com uma entidade empregadora (organização) específica a longo prazo.

#### Pré-condições

* Uma categoria para a dada tarefa.

#### Pós-condições
A informação da tarefa é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador da organização inicia especificação de tarefa.
2. O sistema solicita dados necessários.
3. O colaborador da organização introduz dados solicitado.
4. O sistema mostra a lista de categorias e pede para selecionar uma.
5. O colaborador da organização seleciona a categoria.
6. O sistema apresenta os dados e solicita confirmação.
7. O colaborador da organização confirma os dados apresentados.
8. O sistema analisa os dados submetidos pelo colaborador da organização e informa do sucesso da operação. 


#### Extensões (ou fluxos alternativos)

*a. O colaborador da organização solicita o cancelamento da especificação da  tarefa.

> O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	> 2a. O colaborador da organização não acrescenta os dados. O caso de uso termina.

4b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o colaborador da organização para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	> 2a. O colaborador da organização não altera os dados. O caso de uso termina.

4c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o colaborador da organização para o facto. 
> 2. O sistema permite a sua alteração (passo 3).
> 
	> 2a. O colaborador da organização não altera os dados. O caso de uso termina. 

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados obrigatórios para além dos já conhecidos?
* Quais os dados que em conjunto permitem detetar a duplicação da especificação da tarefa?
* A referência é gerada automaticamente ou é gerada pelo colaborador da organização?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC6_AOO.svg](UC6_AOO.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
|  1. O colaborador inicia a especificação de uma nova tarefa.   		 |	... interage com o utilizador? | CategoriaTarefaUI    |  Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
|  		          |	... coordena o UC?	| CategoriaTarefaController | Controller    |
|  		          |	... cria instância de Tarefa? | Plataforma   | Creator (Regra 1)   |
| 2. O sistema solicita dados necessários (referência, designação, descrição informal, descrição técnica, duração e custo) |          |          |          |
| 3. O colaborador introduz os dados solicitados.  		 |	... guarda os dados introduzidos?  |   Tarefa | Information Expert (IE) - instância criada no passo 1     |
|  		          |	... fornece as categorias existententes?| Plataforma | Creator (Regra 3) |
| 4. O sistema mostra a lista de categorias existentes para que seja selecionada uma. |	...conhece as categorias existentes a listar? | Plataforma | IE: Plataforma tem todas as Categoria |  	
| 5. O colaborador seleciona a categoria em que pretende catalogar a tarefa. |	...guarda a categoria selecionada? | Tarefa | IE: Tarefa catalogado numa Categoria - instância criada no passo 1 |
| 6. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. | ... valida os dados da Tarefa(validação local)? | Tarefa | IE: Tarefa possui os seus próprios dados |
|  		          |	... valida os dados da Tarefa(validação global)?| Organizacao | IE: A Organizacao contém Tarefas |
| 7. O colaborador confirma. |         |         |         |
| 8. O sistema regista os dados e informa o colaborador do sucesso da operação. |	... guarda a Tarefa criada? | Organizacao | IE: No MD a Organizacao possui Tarefa|
|  		          |	... notifica o utilizador? | EpecificarTarefaUI |       |
             
### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Organizacao
 * Plataforma
 * Tarefa
 * Categoria

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EspecificarTarefaUI  
 * EspecificarTarefaController

###	Diagrama de Sequência

![UC6_SD.svg](UC6_SD.svg)

###	Diagrama de Classes

![UC6_CD.svg](UC6_CD.svg)
