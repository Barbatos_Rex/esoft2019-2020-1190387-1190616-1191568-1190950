# UC 10 - Seriar Candidaturas de Anúncio

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador inicia a seriação de candidaturas.O sistema apresenta os possíveis colaboradores participantes e solicita quem participa. O colaborador seleciona os colaboradores participantes. O sistema apresenta os anúncios da organização do colaborador. O colaborador escolhe o anúncio. O sistema apresenta as candidaturas. O colaborador insere as candidaturas seriadas com a sua classificação e a data e hora do registo. O sistema apresenta dados e pede confirmação. O colaborador da organização confirma. O sistema informa do sucesso da operação.

### SSD

![UC10-SSD](UC10_SSD.svg)

### Formato Completo

#### Ator principal

* Colaborador da organização

#### Partes interessadas e seus interesses

* **Organização:** pretende que , de acordo com a forma estipulada, se escolha o melhor candidato (*freelancer*).
* **T4J:** pretende que todas as candidaturas dos *freelancers* sejam avaliadas.
* **Colaborador da organização:** pretende listar e classificar as candidaturas de todos os *freelancers* que efetuaram uma candidatura.
* ***Freelancer*:** pretende ser escolhido para a tarefa.

#### Pré-condições

* Tem que existir pelo menos um anúncio da organização em período de seriação não automático.

#### Pós-condições

* O registo do processo completo de seriação de candidaturas relativas a um anúncio.

#### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador da organização inicia a seriação das candidaturas de um anúncio.  
2. O sistema apresenta a lista de todos os colaboradores da organização.
3. O colaborador da organização seleciona um colaborador.
4. Os passos 2 e 3 repetem-se até estarem todos os colaboradores participantes selecionados.
5. O sistema apresenta todos os anúncios da organização do colaborador da organização.
6. O colaborador da organização seleciona o anúncio da organização que pretende seriar.
7. O sistema apresenta todas as candidaturas de *freelancers* relativas ao anúncio selecionado.
8. O colaborador da organização reorganiza a lista de candidaturas de modo a formular uma lista seriada e com classificações atribuídas. 
9. O sistema solicita a data e hora do registo da lista de candidaturas seriadas.
10. O colaborador da organização introduz a data e a hora do registo.
11. O sistema apresenta a lista e pede confirmação.
12. O colaborador da organização valida a lista.
13. O sistema informa do sucesso da operação e regista o processo(com lista de candidaturas seriada, data do processo, hora do processo e lista colaboradores participantes no processo).

#### Extensões (ou fluxos alternativos)

*a. O colaborador de organização solicita o cancelamento da seriação de anúncio.

> O caso de uso termina.

4a. O sistema não apresenta nenhum anúncio da organização do colaborador.
>   1. O colaborador da organização informa o sistema de tal facto. O caso de uso termina.

7a. O sistema não tem nenhuma candidatura para o anúncio em questão.
>   1. O colaborador da organização informa o sistema de tal facto. O caso de uso termina.

8a. O sistema deteta classificações com a mesma posição.

>   1. O sistema alerta o colaborador para o facto.
>   2. O sistema permite fazer uma nova classificação das candidaturas (passo 8).

    2a. O colaborador não classifica as candidaturas. O caso de uso termina.

#### Requisitos especiais

(enumerar requisitos especiais aplicáveis apenas a este UC)

\-

#### Lista de Variações de Tecnologias e Dados
(enumerar variações de tecnologias e dados aplicáveis apenas a este UC)

\-

#### Frequência de Ocorrência
(descrever com que frequência este UC é realizado)

\- 

#### Questões em aberto

* Qual é a frequência de ocorrência deste caso de uso?
* O que acontece se não existir nenhuma candidatura para o anúncio, depois de expirado o período de apresentação de candidaturas?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC10-MD](UC10_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador da organização inicia a seriação das candidaturas de um anúncio. |	...interage com o utilizador?	 | SeriarAnuncioUI | Pure Fabrication, porque não justifica a atribuição desta funcionaliade a nenhuma classe do modelo de domínio.                              |
|       		 | ...coordena o caso de uso? | SeriarAnuncioController | Controller |
|       		 |	...tem o anúncio? | RegistoAnuncio | HC+LC |
| 2. O sistema apresenta a lista de todos os colaboradores da organização. | ...tem todos os colaboradores da organização? | RegistoOrganizacao | HC+LC |
| 3. O colaborador da organização seleciona um colaborador. | ...guarda temporariamente os colaboradores? | SeriarAnuncioController | Controller  |
| 4. Os passos 2 e 3 repetem-se até estarem todos os colaboradores participantes selecionados. |							 |             |                              |
| 5. O sistema apresenta todos os anúncios da organização do colaborador da organização. | ...tem o anúncio? | RegistoAnuncio | HC+LC |
|       		 |	 |  |  |
| 6. O colaborador da organização seleciona o anúncio da organização que pretende seriar. |	...tem as candidaturas seriadas?	| CandidaturasSeriadas | Information Expert(IE), pois tem todas as informações necessárias para cumprir essa tarefa. |
|                | guarda as candidaturas seriadas?	 | RegistoCandidaturasSeriadas | HC+LC |
| 7. O sistema apresenta todas as candidaturas de *freelancers* relativas ao anúncio selecionado. |	...guarda as candidaturas? | RegistoCandidaturas | HC+LC |              
| 8. O colaborador da organização reorganiza a lista de candidaturas de modo a formular uma lista seriada e com classificações atribuídas. | ...guarda a lista seriada? | CandidaturasSeriadas | IE, pois tem todas as informações necessárias para cumprir essa tarefa. |
|       		 | ...guarda a classificação? | Classificacao | IE, pois tem todas as informações necessárias para cumprir essa tarefa. |
| 9. O sistema solicita a data e hora do registo da lista de candidaturas seriadas.  |  | |  |
| 10. O colaborador da organização introduz a data e a hora do registo. |	...guarda a data e hora do registo?						 |  CandidaturasSeriadas           |  IE, pois tem todas as informações necessárias para cumprir essa tarefa.                            |
|       		 | ...quem valida a lista de candidaturas? | RegistoCandidaturasSeriadas | HC+LC |
| 11. O sistema apresenta a lista e pede confirmação. |							 |             |                              |
| 12. O colaborador da organização valida a lista. |						 |             |                              |
| 13. O sistema informa do sucesso da operação e regista o processo(com lista de candidaturas seriada, data do processo, hora do processo e lista colaboradores participantes no processo). | ...regista o processo? | RegistoCandidaturasSeriadas  | HC+LC  |
|                |	...valida a operação? | RegistoCandidaturasSeriadas  | HC+LC |

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Organizacao
 * Plataforma
 * Anuncio
 * CandidaturasSeriadas
 * Classificacao
 

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * SeriarAnuncioUI  
 * SeriarAnuncioController

###	Diagrama de Sequência

![UC10-SD](UC10_SD.svg)

###	Diagrama de Classes

![UC10-CD](UC10_CD.svg)
