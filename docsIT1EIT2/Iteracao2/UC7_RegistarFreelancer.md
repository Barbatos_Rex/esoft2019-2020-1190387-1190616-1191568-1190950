# UC7 - RegistoFreelancer

## 1. Engenharia de Requisitos

### Formato Breve

O administrativo inicia o registo de um Freelancer. O sistema pede os dados necessários
para o registo (nome, NIF, endereço postal, contacto telefónico, email),
habilitações académicas	(grau, designação do curso, instituição	que	concedeu o grau e média	de curso) e
reconhecimentos	de competências técnicas(data, competencia técnica e grau de proficiencia). O administrativo
introduz os dados pedidos. O sistema apresenta o dados e solicita confirmação. O administrativo
confirma e o sistema regista os dados e informa do sucesso da operação.


### SSD
![UC7_SSD.svg](UC7_SSD.svg)


### Formato Completo

#### Ator principal

O Administrativo.

#### Partes interessadas e seus interesses

* **Organização:** pretende que os *freelancers* possam candidatar-se a uma dada tarefa proposta pela entidade em causa.
* **T4J:** pretende que todos os intervenientes do caso de uso usem as funcionalidades disponibilizadas pela T4J de forma a realizarem os seus objetivos.
* **Freelancer:** pretende que existam tarefas publicadas às quais se possa candidatar.

#### Pré-condições

O freelancer enviar curriculo para os recursos humanos.

#### Pós-condições
Regista o freelancer. Este deve	poder aceder à plataforma.

#### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia o registo de um freelancer. 
2. O sistema pede os dados básicos(nome, NIF, endereço postal, contacto telefónico, email). 
3. O administrativo insere os dados básicos.
4. O sistema pede a habilitação académica(grau, designação do curso, instituição e media).
5. O administrativo insere os dados da habilitação académica.
6. Os pontos 4 e 5 repetem-se até o freelancer não ter mais habilitações.
7. O sistema mostra lista de competencias técnicas e pede os dados do reconhecimento de competências técnicas(data, competencia técnica e grau de proficiencia).
8. O administrativo insere os dados do reconhecimento e escolhe competência.
9. Os pontos 7 e 8 repetem-se até o freelancer não ter mais reconhecimentos de competências.
10. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. 
11. O administrativo confirma os dados.
12. O sistema regista os dados e informa o administrativo do sucesso da operação. 


#### Extensões (ou fluxos alternativos)


*a) O Administrativo solicita o cancelamento do caso de uso.

>        1. O caso de uso termina. 


3a) O Administrativo não introduz dados válidos.
> 1. O Sistema informa que os dados são inválidos.
> 2. O Sistema alerta o administrativo para o facto, permitindo a alteração dos dados.

>         1. O Administrativo não altera os dados. O caso de uso termina. 

3b) O Administrativo introduz dados que já estavas registados na memória.
> 1. O Sistema informa que os dados já estão registados.
> 2. O Sistema alerta o administrativo para o facto, permitindo a alteração dos dados.

>         1. O Administrativo não altera os dados. O caso de uso termina. 

5a) O Administrativo não introduz dados válidos.
> 1. O Sistema informa que os dados são inválidos.
> 2. O Sistema alerta o administrativo para o facto, permitindo a alteração dos dados.
        
>        1. O Administrativo não altera os dados. O caso de uso termina. 

7a. O sistema deteta que a lista de competências técnicas está vazia.
>1. O sistema informa o administrativo de tal facto.  
>2. O sistema permite a especificação de uma nova competência técnica (**[Ver UC4](UC4_EspecificarCT.md)**).  

>        2a. O administrativo não especifica uma competência técnica. O caso de uso termina.

8a) O Administrativo não introduz dados válidos.
> 1. O Sistema informa que os dados são inválidos.
> 2. O Sistema alerta o administrativo para o facto, permitindo a alteração dos dados.
        
>        1. O Administrativo não altera os dados. O caso de uso termina. 

11a) O Administrativo não confirma o Freelancer.
>1. O Sistema pergunta da certeza do administrativo.
>   1. O Administrativo confirma a certeza.       
>   2. O caso de uso termina.
>2. O Administrativo não confirma a certeza.
>   1. Repete o passo 11.

12a) O Sistema não consegue Registo o Freelancer.

>       1. O Sistema informa o Administrativo do imprevisto. O Caso de uso termina.


#### Requisitos especiais
(enumerar requisitos especiais aplicáveis apenas a este UC)

\-

#### Lista de Variações de Tecnologias e Dados
(enumerar variações de tecnologias e dados aplicáveis apenas a este UC)
\-

#### Frequência de Ocorrência
(descrever com que frequência este UC é realizado)

\-

#### Questões em aberto

* Qual é a frequência deste caso de uso?
* Existem mais dados necessários?
* Os dados são todos obrigatórios?
## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC7_MD.svg](UC7_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal  | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
|1. O administrativo inicia o registo de um freelancer. |...interage com o utilizador?|Registo FreelancerUI| Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existente no Modelo de Domínio. |
|            |...que coordena o caso de uso?|RegistoFreelancerController|Controller
|            |...cria a instancia de Freelancer?|Freelancer|IE pois tem todos os mecanismos necessários para desempenhar essa tarefa
|2. O sistema pede os dados básicos(nome, NIF, endereço postal, contacto, email).|							 |             |                              |
|3. O administrativo insere os dados.|...guarda os dados inseridos? |   Freelancer | Information Expert (IE) - instância criada no passo 1: possui os seus próprios dados.     |
|4. O sistema pede as habilitações académicas(grau, designação do curso e instituição). |							 |             |                              |
|5. O administrativo insere os dados das habilitações académicas. |...guarda os dados inseridos? |RegistoFreelancerController|Controller  |
|**6. Os pontos 4 e 5 repetêm-se até o freelancer não ter mais habilitações.** |							 |             |                              |
|7. O sistema pede o reconhecimento de competências técnicas(data, competencia técnica e grau de proficiencia). |							 |             |                              |
|8. O administrativo insere os dados do reconhecimento. |...guarda os dados inseridos? |   Freelancer | Information Expert (IE): possui os seus próprios dados.    |
|**9. Os pontos 6 e 7 repetêm-se até o freelancer não ter mais reconhecimentos.**|							 |             |                              |
| 10. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.  |	...valida os dados do Freelancer (validação local) | Freelancer |  IE. O Freelancer possui os seus próprios dados.|  	
|	 |	...valida os dados do Freelancer(validação global) | Plataforma  | IE: A Plataforma possui Freelancer.  |
|11. O administrativo confirma. |							 |             |                              |
|12. O sistema regista o freelancer e indica do sucesso da operação.|	... guarda o Freelancer? | Plataforma  | IE: No MD a Plataforma possui Freelancer. |         

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Freelancer
 * ReconhecimentoComp
 * HabilitacaoAcad

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * RegistoFreelancerUI  
 * RegistoFreelancerController


###	Diagrama de Sequência

![UC7_SD.svg](UC7_SD.svg)

######Diagrama de Referência

![UC7_SD_RegistaFreelancerComoUtilizador.svg](UC7_SD_RegistaFreelancerComoUtilizador.svg)

###	Diagrama de Classes

![UC7_CD.svg](UC7_CD.svg)
