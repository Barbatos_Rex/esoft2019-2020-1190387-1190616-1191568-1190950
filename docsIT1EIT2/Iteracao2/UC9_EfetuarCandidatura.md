# UC 9 - Efetuar Candidatura

## 1. Engenharia de Requisitos

### Formato Breve
O freelancer candidata-se a um anúncio onde pode ou não ser escolhido.a a É elegido se tiver o grau de proficiência exigido. O freelancer indica o valor pretendido pela tarefa e o número de dias para realizá-la. Pode incluir um texto de apresentação e/ou motivação.

### SSD
![UC9-SSD](UC9_SSD.svg)

### Formato Completo

#### Ator principal

Freelancer

#### Partes interessadas e seus interesses

* **Freelancer:** pretende efetuar a candidatura.
* **Organização:** pretende que freelancers se candidatem as candidaturas/ realização de tarefas  .
* **T4J:** pretende .... .

#### Pré-condições
* Existência de um anúncio. ([Ver UC8](UC8_PublicarTarefa.md)).
* O freelancer ter o grau de proficiência para essa candidatura.

#### Pós-condições

A candidatura é efetuada com sucesso.

#### Cenário de sucesso principal (ou fluxo básico)

(enumerar os passos do fluxo principal intercalando ator<->sistema)

1. O Freelancer Inicia a candidatura.
2. O sistema mostra a lista de anuncios que é permitido fazer a  candidatura
3. O Freelancer Seleciona o anuncio a que se pretende candidatar. 
4. O sistema solicita os dados pretendidos(valor, numero de dias e texto(opcional)).
5. O Freelancer introduz os dados solicitados.. 
6. O sistema apresenta os dados e pede sua confirmação.
7. O Freelancer confirma os dados.
8. O sistema mostra uma mensagem com a operação concluida


#### Extensões (ou fluxos alternativos)

(alternativas globais ao fluxo principal ou específica de um determiando passo)

*a. O freelancer solicita o cancelamento da candidatura.

> O caso de uso termina.

>4a. O grau de proficiencia não é o minimo obrigatorio
> 1. O sistema informa que o grau de proficiencia não é suficiente para de candidatar
    2a. O sistema solicita o cancelamento da candidatura.

5a. Dados mínimos obrigatórios não preenchidos.
> 1. O sistema informa que há dados em falta.
> 2. O sistema permite a introdução dos passos em falta (passo3).
>
    2.a O freelancer nao introduz os dados em falta. O caso de uso termina.

5b. O sistema indentifica dados/conjunto de dados ja exixtentes no sistema.
>1. O sistema informa, o freelancer, que dados ja existe na plataforma.
> 2. O sistema permite a introdução dos passos em falta (passo3).
>
    2.a O freelancer introduz os dados inválidos. O caso de uso termina.
5c. O sistema deteta que há dados inválidos introduzidos.
> 1. O sistema informa que ha dados inválidos.
> 2. O sistema permite a introdução dos passos em falta (passo3).
>
    2.a O freelancer nao introduz os dados em falta. O caso de uso termina.


#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
(enumerar variações de tecnologias e dados aplicáveis apenas a este UC)
\- 

#### Frequência de Ocorrência
(descrever com que frequência este UC é realizado)

\-

#### Questões em aberto
* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* Qual ou quais os dados que iram distinguir os freelancer caso os valores (valor pretendido, número de dias) sejam iguais?
* Qual a frequência de ocorrência deste caso de uso?
## 2. Análise OO


### Excerto do Modelo de Domínio Relevante para o UC

(Apresentar aqui um excerto do MD relevante para este UC)
![UC9-MD](UC9_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O Freelancer Inicia a candidatura.|..interage com o user |RegistoCandidaturaUI|Pure Fabrication: não se justifica atribuir esta responsabilidade a nenhuma classe existente|
| |...coordena o UC?|RegistoCandidaturaController|Controller|
| |...cria a instância Candidatura|Anuncio| Creator (Regra 1): No Md O anuncio tem Candidatura
| |...conhece o freelancer que esta a usar os sistema|SessaoUtilizador|IE: cf. documentação de gestão de utilizadores|
| 2. O sistema mostra a lista de anúncios que é permitido fazer a  candidatura e está em vigor|...conhece os anuncios a que o user pode se candidatar| Plataforma|Ie. a Plataforma possui o Registo Anuncio| 
| | ...conhece todos os anuncios da plataforma|RegistoAnuncio |HC+LC: Conhece todos os Anuncios |
| |...conhece a tarefa?|Anuncio|IE.Conhece a sua tarefa
| | |Tarefa|IE. possui categoria de tarefas
| | |Categoria  de tarefas|IE: tem associada competencia tecnicas obrigatorias
| | |Competencia Tecnica|IE: tem associado um grau de proficiencia 
| 3. O Freelancer seleciona o anuncio a que se pretende candidatar. |...conhece o anuncio a que se candidatou|Anuncio| Ie. O Anuncio possui um gerardor de id's                           |
| 4. O sistema solicita os dados pretendidos(valor, numero de dias e texto(opcional)).||||
| 5. O Freelancer introduz os dados solicitados. |...guarda os dados introduzidos	 | Candidatura | Information Expert(IE)- instancia criada no passo 1                    |
| 6. O sistema apresenta os dados e pede sua confirmação.|...valida os dados da Candidatura|Candidatura|IE: CAndidatura possui os seus dados proprios|
| 7. O Freelancer confirma os dados.||||
| 8. O sistema mostra uma mensagem com a operação concluida|...guarda a Candidatura criada?			 |ListaCandidatura             |HC+LC: a ListaCandidatura armazena todas as candidaturas respetivas ao Anuncio                          |              

|
### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * RegistoAnuncio
 * Freelancer
 * RegistoFreelancer
 * Plataforma
 * Anuncio
 * RegistoCandidatura
 * Candidatura
 

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * RegistoCandidaturaUI  
 * RegistoCandidaturaController

Outras classes de sistema/componentesexternos:
* SessaoUtilizador
* AplicaoPOT
###	Diagrama de Sequência

![UC9_SD.svg](UC9_SD.svg)


###	Diagrama de Classes

![UC9_CD.svg](UC9_CD.svg)
