# UC 8 - Publicar Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador da organização inicia o pedido de publicação de tarefa. O sistema 
pede os dados necessários para a tal tarefa. O colaborador fornece todos os dados necessários (i.e 1 tarefa, um regime de seriação, 3 períodos, 1 de publicação, outro de candidaturas e outro de decisão).
O sistema apresenta o anúncio e pede confirmação. O colaborador confirma. O Sistema retorna sucesso.


### SSD
![UCXX-SSD](UC8_SSD.svg)


### Formato Completo

#### Ator principal

O Colaborador da Organização.

#### Partes interessadas e seus interesses


* **Organização:** pretende que os *freelancers* possam candidatar-se a uma dada tarefa proposta pela entidade em causa.
* **T4J:** pretende que todos os intervenientes do caso de uso usem as funcionalidades disponibilizadas pela T4J de forma a realizarem os seus objetivos.
* **Colaborador da Organização:** pretende que a organização publique uma tarefa para ser realizada por um *freelancer*.
* **Freelancer:** pretende que existam tarefas publicadas às quais poderá se candidatar.
<!-- * **Parte Interessada N:** pretende .... . --->

#### Pré-condições

- Uma tarefa pré-existente na Organização em causa ([Ver UC6](UC6_EspecificarTarefa.md)).

#### Pós-condições
A tarefa é publicada com sucesso.

#### Cenário de sucesso principal (ou fluxo básico)

1. O Colaborador da Organização inicia o processo de criação do anúncio.
2. O Sistema mostra quais as tarefas que podem ser publicadas pelo colaborador.
3. O Colaborador escolhe 1 das tarefas apresentadas pelo sistema.
4. O Sistema pede os períodos de publicação,candidatura e decisão.
5. O Colaborador introduz os dados pedidos.
6. O Sistema apresenta os tipos de regimento possíveis.
7. O Colaborador escolhe um dos tipos de regimento.
8. O Sistema cria o anúncio, mostra ao colaborador e pede confirmação.
9. O Colaborador confirma o anúncio.
10. O Sistema guarda o anúncio e publica o mesmo, informando o sucesso do caso de uso.


#### Extensões (ou fluxos alternativos)

*a) O Colaborador solicita o cancelamento do caso de uso.
>1. O caso de uso termina. 

2a) O Colaborador não tem nenhuma tarefa para ser publicada.
>1. O Sistema informa o utilizador que não tem nenhuma tarefa criada.
>2. O Sistema pergunta ao colaborador se quer criar uma nova tarefa.
>   1. O colaborador não cria uma nova tarefa.
>   2. O caso de uso termina

5a) O Colaborador não introduz dados válidos.
> 1. O Sistema informa que os dados são inválidos.
> 2. O Sistema alerta o colaborador para o facto, permitindo a alteração dos dados.
>   1. O Colaborador não altera os dados.
>   2. O caso de uso termina. 

6a) O Sistema não consegue criar o anúncio.
>1. O Sistema informa o Colaborador do imprevisto.
>2. O Caso de uso termina.

6b) O anúncio criado é inválido.
>1. O Sistema informa o Colaborador do imprevisto.
>2. O Caso de uso termina.

7a) O Colaborador não confirma o anúncio criado.
>1. O anúncio pergunta da certeza do colaborador.
>   1. O Colaborador confirma a certeza.       
>   2. O caso de uso termina.
>2. O Colaborador não confirma a certeza.
>   1. Repete o passo 7.

8a) O Sistema não conseguiu publicar o anúncio.
>1. O Sistema informa o Colaborador do imprevisto.
>2. O Caso de uso termina.
>
#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência

\-

#### Questões em aberto

* Qual é a frequência deste caso de uso?
* Existem mais dados necessários?
* Os dados são todos obrigatórios?
* Este caso de uso coordena os métodos de seriação?
* Pode existir múltiplos períodos de seriação?
* O período de decisão deve ser pedido separadamente do período de seriação?



## 2. Análise OO
### Excerto do Modelo de Domínio Relevante para o UC
![UC8_MD](UC8_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O Colaborador da Organização inicia o processo de criação do anúncio.| ...interage com o utilizador?| PublicarTarefaUI|Pure Fabrication pois não se justifica atribuir esta responsabilidade a outra classe do modelo de domínio.                            |
| |...que coordena o caso de uso?|PublicarTarefaController| Controller|
| |...cria o anuncio?| Anuncio| IE pois tem todos os mecanismos necessários para desempenhar essa tarefa|
| |...gera o id para o anuncio?| Anuncio |IE pois o anuncio tem todos os dados necessários para gerar o respetivo id
| 2. O Sistema mostra quais as tarefas que podem ser publicadas pelo colaborador.|...tem as tarefas?| RegistoTarefa| HC+LC 
| 3. O Colaborador escolhe 1 das tarefas apresentadas pelo sistema.| ...guarda temporariamente a tarefa?| PublicarTarefaController|Controller|
| 4. O Sistema pede os períodos de publicação,candidatura e decisão.| ...define os períodos?| Periodo| HC+LC|
| |...cria os períodos? | Anuncio | Creator(Regra 1)
| 5. O Colaborador introduz os dados pedidos.|...guarda os dados?| Anuncio| IE (Ver passo 1)                               |
| 6. O Sistema apresenta os tipos de regimento possíveis.|...tem os possíveis tipos de regimento?| Seriacao| IE pois contém todos os tipos de regimentos possíveis|
| 7. O Colaborador escolhe um dos tipos de regimento.| ...guarda o tipo de regimento?| Anuncio | IE (Ver paço 1)
| 8. O Sistema cria o anúncio, mostra ao colaborador e pede confirmação. |...verifica o se o anuncio está correto?|RegistoAnuncio| HC+LC
| 9. O Colaborador confirma o anúncio.
| 10. O Sistema guarda o anúncio e publica o mesmo, informando o sucesso do caso de uso.|...guarda o anuncio?|RegistoAnuncio| HC+LC
| |...verifica o anuncio?| Organizacao|Ver passo 8


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Organizacao
 * Anuncio
 * Regimento
 * Plataforma

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * PublicarTarefaUI  
 * PublicarTarefaController
 * RegistoAnuncio
 * RegistoTarefa
 * Data


###	Diagrama de Sequência

![SD_UC8.svg](UC8_SD.svg)


###	Diagrama de Classes

![CD_UCX.png](UC8_CD.svg)
